<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
	<script type="text/javascript">
		popContextMenuFlg=0;
		var custContextMenuFlg=0;
	</script>
		<TITLE>欢迎登录MYPM</TITLE>
		<link rel="shortcut icon"  type="image/x-icon" href="<%=request.getContextPath()%>/images/go.gif" />
		<link rel="Bookmark" type="image/x-icon"  href="<%=request.getContextPath()%>/images/go.gif" />
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/css/page.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
	    <script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
	   <link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	   <link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	</HEAD>
	<%
	cn.com.mypm.framework.security.Visit visit =(cn.com.mypm.framework.security.Visit)session.getAttribute("currentUser");
	cn.com.mypm.framework.security.VisitUser vu = visit.getUserInfo();
	String uName = vu.getLoginName() + "(" + vu.getName() + ")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + cn.com.mypm.common.util.StringUtils.formatShortDate(new java.util.Date());
	%>
	<script type="text/javascript">
	popContextMenuFlg=0;
	<pmTag:priviChk urls="taskAction!taskLists" varNames="canViewTodayTask"/>
	</script>
	<BODY bgcolor="#F8F8FF"  oncontextmenu="return false">
	<table cellspacing="0" cellpadding="0" align="center" border="0">
		<tr>
			<td>
				<div id="logoDiv" align="center" style="border:0px;width:100%;">
					<table id="logoTable" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td height="50" background="<%=request.getContextPath()%>/jsp/common/images/logo/border.jpg"  valign="top">
								<img border="0" src="<%=request.getContextPath()%>/jsp/common/images/logo/mypmLogo.jpg"/>
							</td>
							<td height="50" background="<%=request.getContextPath()%>/jsp/common/images/logo/border.jpg" align="center">
								<div style="padding-top:30px;text-align:right;font-size:12px;font-weight:bolder;color:white;">欢迎您:&nbsp;<%=uName%>&nbsp;&nbsp;</div>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr id="msgTr">
		  <td>
		  	<table cellspacing="0" cellpadding="0" align="center" border="0">
		  		<tr>
		  			<td>
		  				<div id="msgDivTitle" class="gridbox_light" style="border:0px;width:100%;display:none">
							<div class="xhdr" style="overflow:hidden;height:27px;position:relative;">
								<table class="hdr" cellspacing="0" cellpadding="0" style="width:100%;table-layout:fixed;">
									<tr style="height:27"><td><div class="hdrcell" align="left">我的消息</div></td></tr>
								</table>
							</div>
						</div>	
		  			</td>
		  		</tr>
		  		<tr>
		  			<td valign="top"><div id="mypmHomeMsgDiv" style="display:none"></div></td>
		  		</tr>
		  		<tr>
		 			<td>&nbsp;</td>
				</tr>
		  	</table>
		  </td>
		</tr>
		<tr id="bugTr">
		  <td>
		  	<table cellspacing="0" cellpadding="0" align="center" border="0">
		  		<tr>
		  			<td>
						<div id="bugTitle" class="gridbox_light" style="border:0px;width:100%;display:none">
							<div class="xhdr" style="overflow:hidden;height:27px;position:relative;">
								<table class="hdr" cellspacing="0" cellpadding="0" style="width:100%;table-layout:fixed;">
									<tr style="height:27"><td><div class="hdrcell" align="left">我的Bug</div></td></tr>
								</table>
							</div>
						</div>	
		  			</td>
		  		</tr>
		  		<tr>
		  			<td valign="top"><div id="bugDiv" style="display:none"></div></td>
		  		</tr>
		  		<tr>
		 			<td>&nbsp;</td>
				</tr>
		  	</table>
		  </td>
		</tr>
		<tr id="taskTr">
		  <td>
		  	<table cellspacing="0" cellpadding="0" align="center" border="0">
		  		<tr>
		  			<td>
						<div id="tasksTitle" class="gridbox_light" style="border:0px;width:100%;display:none">
							<div class="xhdr" style="overflow:hidden;height:27px;position:relative;">
								<table class="hdr" cellspacing="0" cellpadding="0" style="width:100%;table-layout:fixed;">
									<tr style="height:27"><td><div class="hdrcell" align="left">今日任务</div></td></tr>
								</table>
							</div>
						</div>
		  			</td>
		  		</tr>
		  		<tr>
		  			<td valign="top"><div id="taskDiv" style="display:none"></div></td>
		  		</tr>
		  		<tr>
		 			<td>&nbsp;</td>
				</tr>
		  	</table>
		  </td>
		</tr>
		<tr><td>&nbsp;</td></tr>	
		<tr> 
		 <td valign="top" align="right"><img alt="进入我的BUG列表" title="进入我的BUG列表" src="<%=request.getContextPath()%>/dhtmlx/toolbar/images/goProLst.gif" onclick="javascript: goProLst()"></td> 
		</tr>
		<tr>
			<td class="tdtxt" align="center"  style="border-right:0">
				<div id="hintMsgTxt" align="center" style="color: Blue"></div>
			</td>
		</tr>
	</table>
	<input type="hidden" id="listStr" name="listStr" value=""/>
		<div id="msgDetalDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		   <div class="objbox" style="overflow:auto;width:100%;">
			 <input type="hidden" id="initContent" name="initContent"/>
			 <input type="hidden" id="startDateInit" name="startDateInit"/>
			 <input type="hidden" id="sendName" name="sendName"/>
			 <input type="hidden"  id="attachUrl" name="dto.broMsg.attachUrl"/>
			<ww:form theme="simple" method="post" id="createForm"name="createForm" namespace="" action="">
			<input type="hidden"  id="logicId" name="dto.broMsg.logicId"/>
			<input type="hidden" id="state" name="dto.broMsg.state"/>
			<input type="hidden"  id="senderId" name="dto.broMsg.senderId"/>
			<input type="hidden"  id="sendDate" name="dto.broMsg.sendDate"/>
			<input type="hidden"  id="recpiUserId" name="dto.broMsg.recpiUserId"/>
			<input type="hidden"  id="attachUrl" name="dto.broMsg.attachUrl"/>
			<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%" id="msgDetalTab">
			<tr>
			  <td width="600"colspan="4" style="border-right:0">&nbsp;
			  </td>
			</tr>
			  <tr>
			  	 <td class="rightM_center" width="80" align="right">
			  	 标题:
			  	 </td>
			    <td class="dataM_left" width="560" colspan="3"align="left">
					<ww:textfield id="msgTitle" name="dto.broMsg.title" readonly="true" cssStyle="width:560;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			    
			  </tr>	
			  <tr>
			  	 <td class="rightM_center" align="right"width="80" nowrap style="border-right:0">
			  	 生效日期: 
			  	 </td>
			    <td class="dataM_left" width="100" align="left" style="border-right:0">
					<ww:textfield id="startDate" readonly="true" name="dto.broMsg.startDate" cssStyle="padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  	 <td class="rightM_center" width="80" align="right" style="border-right:0">
			  	 失效日期: 
			  	 </td>
			    <td class="dataM_left" width="100" align="left" style="border-right:0">
					<ww:textfield id="overdueDate" readonly="true" name="dto.broMsg.overdueDate" cssStyle="padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr>
			  	 <td class="rightM_center" width="80" align="right">
			  	 类型: 
			  	 </td>
			    <td class="dataM_left" width="100" align="left">
					<ww:select id="msgType" name="dto.broMsg.msgType"
						list="#{-1:'',0:'广播',1:'限定接收人'}" headerValue="-1" cssStyle="width:100;" cssClass="text_c">
					</ww:select>
			    </td>
			  	 <td class="rightM_center" width="80" align="right">
			  	 同时Mail告知:
			  	 </td>
			    <td  width="100" align="left" class="dataM_left">
			        <ww:checkbox name="dto.broMsg.mailFlg" id="mailFlg" disabled="true" value="FLASE" fieldValue="1" cssClass="text_c"/>
			    </td>
			  </tr>	
			  <tr id="recpiUserIdTr" style="display:none">
			  	 <td class="rightM_center" width="80" align="right">
			  	 接收人: 
			  	 </td>
			    <td class="dataM_left" width="560" colspan="3"align="left">
					<ww:textfield id="recpiUserName" name="recpiUserName"readonly="true"   cssStyle="width:560;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr>
			  	 <td class="rightM_center" width="80" align="right">
			  	 内容: 
			  	 </td>
			    <td class="dataM_left" width="560" colspan="3"align="left">
					<textarea name="dto.broMsg.content" id="content" cols="50"
							rows="30" style="width:560;hight:200;padding:2 0 0 4;"  Class="text_c">
					</textarea>	
			    </td>
			  </tr>
			<tr>
			<td class="rightM_center" width="80" align="right">附件:</td>
			  <td width="560"colspan="3" align="left"><img src="<%=request.getContextPath()%>/images/button/attach.gif"  style="display:none"id="currAttach" alt="当前附件" title="打开附件" onclick="openAtta()" />
			  </td>
			</tr>
			<tr>
				<td class="tdtxt" align="center" width="640" colspan="4">
					<a class="bluebtn" href="javascript:void(0);"
						onclick="javascript:msgDetalW_ch.setModal(false);msgDetalW_ch.hide();"
						style="margin-left: 6px"><span> 返回</span> </a>
				<td>
			</tr>	
			<tr>
			  <td width="600"colspan="4">&nbsp;
			  </td>
			</tr>			
			</table>
			</ww:form>
		   </div>
		</div>	
	<form action="downloadfile" method="post" id="downForm" name="downForm">
		<input type="hidden" id="downloadFileName" name="downloadFileName" value=""/>
   </form>	
	<script type="text/javascript">
	var myHome = "<%=session.getAttribute("myHome")%>";
	if(myHome==""||myHome=="null"){
		var result = dhtmlxAjax.postSync(conextPath + "/commonAction!getMyHome.action","").xmlDoc.responseText;
		if(result!="failed"){
			myHome = result;
		}else{
			$("hintMsgTxt").innerHTML="加载我的主页发生错误";
		}
	}
	</script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/ext/dhtmlxwindows_wtb.js"></script>	
	<ww:include value="/jsp/common/message.jsp"></ww:include>
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/myhome/myHome.js"></script>
	 <ww:include value="/jsp/common/setMyPC.jsp"></ww:include>

	</BODY>
</HTML>
