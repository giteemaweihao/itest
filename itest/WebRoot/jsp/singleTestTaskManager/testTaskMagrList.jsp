<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>测试项目</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>			
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>			
	</HEAD>
	<BODY bgcolor="#ffffff" style="overflow-x:hidden;">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top" style="padding-right: 3px;">
					<div id="toolbarObj"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox"></div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}"/>
		<ww:hidden id="reProStep" name="reProStep"></ww:hidden>
	<script type="text/javascript">
	ininPage("toolbarObj", "gridbox", 770);
	<pmTag:button page="singleTestTask" find="true" checkAll="false"/>
	<pmTag:priviChk urls="testTaskManagerAction!flwSetInit,testTaskManagerAction!update" varNames="canBrowser,canUpd"/>
	</script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/singleTestTaskManager/testTaskMagr.js"></script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<table id="createTable" class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
			<ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="" action="">
			<ww:hidden id="taskId" name="dto.singleTest.taskId"></ww:hidden>
			<ww:hidden id="insDate" name="dto.singleTest.insDate"></ww:hidden>
			<ww:hidden id="updDate" name="dto.singleTest.updDate"></ww:hidden>
			<ww:hidden id="createId" name="dto.singleTest.createId"></ww:hidden>
			<ww:hidden id="status" name="dto.singleTest.status"></ww:hidden>
			<ww:hidden id="attachUrl" name="dto.singleTest.planDocName"></ww:hidden>
			<tr class="ev_mypm">
				<td colspan="4"  style="border-right:0;width:400">
					<div id="cUMTxt" align="center" style="color: Blue; padding: 2px">
						&nbsp;
					</div>
				</td>
			</tr>
			  <tr class="odd_mypm" >
			  	 <td class="rightM_center" style="color:red;width:80;border-right: 0" nowrap>
			  	 项目编号:
			  	 </td>
			    <td class="dataM_left" style="width:120;border-right: 0">
					<ww:textfield id="proNum" maxlength="15" name="dto.singleTest.proNum" cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  	 <td class="rightM_center" style="color:red;width:60;border-right: 0" nowrap>
			  	 项目名称:
			  	 </td>
			    <td class="dataM_left" style="width:140;border-right: 0">
					<ww:textfield id="proName" maxlength="30" name="dto.singleTest.proName" cssStyle="width:140;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr class="ev_mypm" nowrap>
			  	 <td class="rightM_center" style="color:red;width:80;border-right: 0">
			  	 测试阶段:
			  	 </td>
			    <td class="dataM_left" style="width:120;border-right: 0">
					<ww:select id="testPhase" name="dto.singleTest.testPhase"
						list="#{-1:'',0:'单元测试',1:'集成测试',2:'系统测试'}" headerValue="-1" cssStyle="width:120;" cssClass="text_c">
					</ww:select>
			    </td>
			  	 <td class="rightM_center" style="color:red;width:60;border-right: 0" nowrap>
			  	 研发部门:
			  	 </td>
			    <td class="dataM_left" style="width:140;border-right: 0">
					<ww:textfield id="devDept" maxlength="30" name="dto.singleTest.devDept" cssStyle="width:140;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr class="odd_mypm" nowrap>
			  	 <td class="rightM_center" style="color:red;width:120;border-right: 0">
			  	 开始日期:
			  	 </td>
			    <td class="dataM_left"style=";border-right: 0">
					<ww:textfield id="planStartDate" readonly="true" name="dto.singleTest.planStartDate" cssClass="text_c" cssStyle="padding:2 0 0 4;" onclick="initDhtmlxCalendar(); showCalendar(this);"> </ww:textfield>			    
			    </td>
			  	 <td class="rightM_center" style="color:red;width:120;border-right: 0" align="right" nowrap>
			  	 结束日期:
			  	 </td>
			    <td class="dataM_left" style="border-right:0">
					<ww:textfield id="planEndDate" readonly="true" name="dto.singleTest.planEndDate"  cssClass="text_c" cssStyle="padding:2 0 0 4;" onclick="initDhtmlxCalendar(); showCalendar(this);"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr class="ev_mypm">
			  	 <td class="rightM_center" style="color:red;width:80;border-right: 0">
			  	 PM:
			  	 </td>
			    <td class="dataM_left"  style="width:120;border-right: 0">
			    	<ww:hidden id="psmId" name="dto.singleTest.psmId"></ww:hidden>
					<ww:textfield id="psmName" name="dto.singleTest.psmName" readonly="true" onfocus="popselWin('psmId','psmName',true);" cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			    <td class="dataM_left" colspan="2" style="width:200;border-right: 0">&nbsp;
			    </td>
			  </tr>	
			
			</ww:form>
		    <ww:form enctype="multipart/form-data" theme="simple" name="fileform" id="fileform" action="" method="post" target="target_upload">
			  <tr class="odd_mypm">
			  	 <td class="rightM_center" width="80" style="border-right:0">
			  	 测试计划:
			  	 </td>
			    <td class="dataM_left"  colspan="2" width="374" style="border-right:0">
					<input name="currUpFile" id="currUpFile" type="file" style="padding:2 0 0 4;width:210" Class="text_c">
					<img src="<%=request.getContextPath()%>/images/button/attach.gif" style="display:none"id="currAttach" alt="当前附件" title="查看测试计划" onclick="openAtta()" />
			    </td>
			    <td style="border-right:0">
			    </td>
			  </tr>	
			  <tr class="ev_mypm">
			    <td width="80" align="right" style="border-right:0">&nbsp;</td>
				<td width="320" style="width:200;padding:2 0 0 4;border-right:0" colspan="3"><div id="upStatusBar"></div></td>		  
			  </tr>		  
			<tr  class="odd_mypm">
				<td class="dataM_center" align="center" width="400" colspan="4" style="border-right:0">
					<a class="bluebtn" href="javascript:void(0);"
						onclick="javascript:closeMe(cuW_ch);"
						style="margin-left: 6px;"><span> 返回</span> </a>
					<a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="upLoadAndSub('addUpSub','addSubCheck',0);"
						style="margin-left: 6px;"><span> 确定</span> </a>
					<a class="bluebtn" href="javascript:void(0);"
						onclick="javascript: getTemplate('testPlanTemplate.xls');"
						style="margin-left: 6px;"><span> 下载测试计划模板</span> </a>
				<td>
		  </tr>	
		  <tr class="ev_mypm">
		    <td colspan="4" style="border-right:0">&nbsp;</td>
		  </tr>	
		  </ww:form>		  
 		     <iframe id="target_upload" name="target_upload" src="" frameborder="0" scrolling="no" width="0" height="0"></iframe>
	       </table>
	      </div>
	    </div>
		<div id="findDiv"  class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findForm" name="findForm"
				namespace="/singleTestTask" action="">
				<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="odd_mypm" >
						<td align="right" class="rightM_center">项目编号:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="proNum_f" name="dto.singleTest.proNum"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" class="rightM_center">项目名称:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="proName_f" name="dto.singleTest.proName"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" class="rightM_center" style="border-right:0;">研发部门:</td>
						<td style="border-right:0;">
							<ww:textfield id="devDept_f" name="dto.singleTest.devDept"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" style="border-right:0;" class="rightM_center">状态:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:select id="status_f" name="dto.singleTest.status"
								list="#{-1:'',0:'进行',1:'完成',2:'结束',2:'准备'}" headerValue="-1" cssStyle="width:120;" cssClass="text_c">
							</ww:select>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="dataM_left" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" align="right">
							<a class="bluebtn" href="javascript:void(0);" id="quBtn"onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="quU_b"onclick="findExe();"
								style="margin-left: 6px;"><span> 查询</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="qretBtn"onclick="formReset('findForm');"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4">&nbsp;</td>
					</tr>
				</table>
			</ww:form>
		</div>
		</div>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.css">	
   	<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.js"></script>
	<ww:include value="/jsp/common/selSingleCompanyPerson.jsp"></ww:include>
	<ww:include value="/jsp/common/downLoad.jsp"></ww:include>
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</BODY>
	<script type="text/javascript">
		importJs(conextPath+"/jsp/common/upload.js");
	</script>
</HTML>
