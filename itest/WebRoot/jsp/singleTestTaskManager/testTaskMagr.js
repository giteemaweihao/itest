	contin='0';
	pmBar.attachEvent("onClick", function(id){
	if(id =="new"){
	    	addUpInit("new");
	   }else if(id =="upd"){
	   		if(pmGrid.getSelectedId()==null){
	   			hintMsg("请选择要修改的记录");
	   			return;
	   		}
	    	addUpInit("upd");
	   }else if(id =="del"){
	   		if(pmGrid.getSelectedId()==null){
	   			hintMsg("请选择要删除的记录");
	   			return;
	   		}
	   		cfDialog("delExe","您确定删除选择的记录?",false);	    	
	   }else if(id =="find"){
	    	findInit();
	   }else if(id == "first"){
			pageAction(1, pageSize);
		}else if(id == "last"){
			pageAction(pageCount, pageSize);
		}else if(id == "next"){
			pageAction(pageNo + 1, pageSize);
		}else if(id == "pervious"){
			pageAction(pageNo -1, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,项目编号,项目名称,研发部门,测试阶段,项目PM,计划开始时间,计划结束时间,测试计划,状态,测试负责人,&nbsp;");
    pmGrid.setInitWidths("40,40,80,180,80,80,80,90,90,70,40,*,0");
    pmGrid.setColAlign("center,center,left,left,left,center,left,center,center,center,center,left,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str");
	//pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,true,true,true,true,true,true,true,true,true,true,true,false");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr");
    pmGrid.attachEvent("OnCheck",doOnCheck);
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		if(pmGrid.cells(rowId,12).getValue()=="1"){
			try{
			pmBar.disableItem("upd");
			}catch(err){}
			try{
				pmBar.disableItem("del");
			}catch(err){}
		}else{
			try{
				pmBar.enableItem("upd");
			}catch(err){}
			try{
				pmBar.enableItem("del");
			}catch(err){}		
		}
		return true;
	}
	
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
		pmGrid.setSelectedRow(rowId);
		if(pmGrid.cells(rowId,12).getValue()=="1"){
			try{
			pmBar.disableItem("upd");
			}catch(err){}
			try{
				pmBar.disableItem("del");
			}catch(err){}
		}else{
			try{
				pmBar.enableItem("upd");
			}catch(err){}
			try{
				pmBar.enableItem("del");
			}catch(err){}		
		}
	}
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
	}
	function getTttle(rowId,colIn){
		return pmGrid.cells(rowId,colIn).getValue();
	}
	loadLink();
	function loadLink(){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			
			if(getTttle2(i,9)!=""&&getTttle2(i,9).indexOf("<a href")<0){
				pmGrid.cells2(i,9).cell.innerHTML="<img src='"+conextPath+"/images/button/attach.gif' alt='附件'  title='查看测试计划' onclick=\"openAtta('"+getTttle2(i,9)+"')\"/>";
			}
			if((canBrowser|| canUpd)&&getTttle2(i,3).indexOf("<a href")<0){
				pmGrid.cells2(i,3).cell.innerHTML="<a href='javascript:setFlwInit()' title='设置(查看)测试流程--"+getTttle2(i,3)+"'>"+getTttle2(i,3)+"</a>";
			}
			if(getTttle2(i,12)!="0"&&getTttle2(i,6).indexOf("<a href")<0){
				pmGrid.cells2(i,6).cell.innerHTML="<a href='javascript:void(0)' title='纳入项目管理的测试任务(如标识为蓝色表示独立测试项目)--"+getTttle2(i,6)+"'><font color='#339933'>"+getTttle2(i,6)+"</font></a>";
			}else{
			   if(getTttle2(i,6).indexOf("<a href")<0){
			       pmGrid.cells2(i,6).cell.innerHTML="<a href='javascript:void(0)' title='不纳入项目管理的独立测试项目--"+getTttle2(i,6)+"'>"+getTttle2(i,6)+"</a>";
			   }
		    }	       
		}
		sw2Link();
	}
	function colTypeReset(){
		pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		if(canBrowser||canUpd){
			pmGrid.setColTypes("ra,ro,ro,link,ro,ro,link,ro,ro,link,ro,ro,ro");
         }else{
            pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,link,ro,ro,link,ro,ro,ro");
         }
	}


	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount && pageSizec<1){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url = conextPath+"/singleTestTask/singleTestTaskAction!magrTaskList.action?dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		var userJson = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		pmGrid.clearAll();
	   		colTypeReset();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		loadLink();
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum(pmGrid);
   		}

	}	
	function formRest(){
		$("taskId").value="";
		$("attachUrl").value="";
		$("currUpFile").value="";
		$("insDate").value="";
		$("updDate").value="";
		$("createId").value="";
		$("status").value="";
		$("currAttach").style.display="none";
		$("currUpFile").style.display="";
		$("createForm").reset();
		$("cUMTxt").innerHTML="";
		upVarReset();	
	}
	function addUpInit(id){
		formRest();
		$("cUMTxt").innerHTML="";
		if(id=="upd"){
			var url = conextPath+"/singleTestTask/singleTestTaskAction!updInit.action?dto.singleTest.taskId="+pmGrid.getSelectedId();
			var rest= dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
			if(rest=="failed"){
				hintMsg("加载数据发生错误");
				return;
			}else if(rest=="delete"){
				hintMsg("所选记录己被删");
				return;
			}else if(rest.indexOf("success")>=0){
				setUpInfo(rest.split("$")[1]);
			}
		}
		
		if(navigator.userAgent.indexOf("Chrome")>0)
			cuW_ch = initW_ch(cuW_ch, "createDiv", true, 540, 240);
		else
			cuW_ch = initW_ch(cuW_ch, "createDiv", true, 540, 200);
		cuW_ch.button("close").attachEvent("onClick", function(){
			 closeMe(cuW_ch);
		});
		if(id=="new"){
			cuW_ch.setText("新建测试项目");
		}else{
			cuW_ch.setText("修改测试项目");
		}
		cuW_ch.show();
		cuW_ch.setModal(true);
		cuW_ch.bringToTop();
	}
	function closeMe(winObj){
		winObj.setModal(false);
		winObj.hide();	
		if(typeof(mypmCalendar_ch)!='undefined')
			mypmCalendar_ch.hide();
		if(typeof(comUserSelW_ch)!='undefined'){
			comUserSelW_ch.setModal(false);
			comUserSelW_ch.hide();		
		}
	}
	function delExe(){
		var url = conextPath+"/singleTestTask/singleTestTaskAction!delete.action?dto.singleTest.taskId="+pmGrid.getSelectedId();
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="failed"){
			hintMsg("删除时发生错误");
		}else{
			pmGrid.deleteRow(pmGrid.getSelectedId());
			clsoseCfWin();
		}
		
	}
	function addUpSub(){
		if(addSubCheck()){
			var url = conextPath+"/singleTestTask/singleTestTaskAction!add.action";
			if($("taskId").value!="")
				url = conextPath+"/singleTestTask/singleTestTaskAction!update.action";
			var rest = dhtmlxAjax.postSync(url, "createForm").xmlDoc.responseText;
			if(rest.indexOf("success")>=0){
				$("cUMTxt").innerHTML="";
				var rowData = rest.split("$")[1];
				colTypeReset();
				if($("taskId").value=="")
					pmGrid.addRow(rowData.split("^")[0],rowData.split("^")[1],0);
				else{
				   pmGrid.deleteRow(pmGrid.getSelectedId());
				   pmGrid.addRow(rowData.split("^")[0],rowData.split("^")[1],0);
				 }
				setRowNum(pmGrid);
				loadLink();
				upVarReset();
				cuW_ch.setModal(false);
				cuW_ch.hide();
				return;
			}
			$("cUMTxt").innerHTML="保存数据时发错错误";
			hintMsg("保存数据时发错错误");
		}
	}

	function addSubCheck(){
		if(isWhitespace($("proNum").value)){
			$("cUMTxt").innerHTML="项目编号不能为空";
			return false;
		}
		if(isWhitespace($("proName").value)){
			$("cUMTxt").innerHTML="项目名称不能为空";
			return false;
		}
		if($("testPhase").value=='-1'){
			$("cUMTxt").innerHTML="测试阶段不能为空";
			return false;
		}
		if(isWhitespace($("devDept").value)){
			$("cUMTxt").innerHTML="研发部门不能为空";
			return false;
		}
		if(isWhitespace($("planStartDate").value)){
			$("cUMTxt").innerHTML="计划开始日期不能为空";
			return false;
		}
		if(isWhitespace($("planEndDate").value)){
			$("cUMTxt").innerHTML="计划结束日期不能为空";
			return false;
		}
		var startDate = $("planStartDate").value;
		var endDate = $("planEndDate").value;
		endDate = new Date(endDate);
		startDate = new Date(startDate);
		if((startDate.getTime() - endDate.getTime())>=0){
			hintMsg("计划结束日期必须在计划开始日期之后");
			return false;				 
		}		
		if(isWhitespace($("psmId").value)){
			$("cUMTxt").innerHTML="PSM不能为空";
			return false;
		}
		return true;
	}

	function setUpInfo(updInfo){
		var updInfos = updInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1],'notRepComma');
				valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				eval(valueStr);
				if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
					$("currAttach").style.display="";
					$("currAttach").title="附件";
					//$("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);
				}
			}
		}
	}
	function findInit(){
		fW_ch = initW_ch(fW_ch, "findDiv", true, 500, 135);
		fW_ch.setText("查询");
		fW_ch.show();
		fW_ch.bringToTop();
	}
	function findExe(){
		var url = conextPath+"/singleTestTask/singleTestTaskAction!magrTaskList.action";
		var userJson = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText.split("$");
		if(userJson[0]=="failed"){
			hintMsg("查询发重错误");
			return;
		}
		if(userJson[1] == ""){
			pmGrid.clearAll();
			setPageNoSizeCount(userJson[0]);
		}else{
	   		pmGrid.clearAll();
	   		colTypeReset();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		loadLink();
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum(pmGrid);
	   		fW_ch.setModal(false);
	   		fW_ch.hide();
   		}
	}
	
	var testW_ch;
	function setFlwInit(){
		testW_ch = initW_ch(testW_ch, "", true, 550, 510, 'testW_ch');
		testW_ch.setText("设置测试流程");
		var taskId = pmGrid.getSelectedId();
		var url = conextPath + "/testTaskManager/testTaskManagerAction!flwSetInit.action?dto.taskId=" + taskId +"&dto.taskType="+pmGrid.cells(taskId,12).getValue()+"&dto.comeFrom=flwSetList";
		testW_ch.attachURL(url);
	}