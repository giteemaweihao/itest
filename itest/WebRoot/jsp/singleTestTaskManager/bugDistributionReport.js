	contin='0';
	pmBar.attachEvent("onClick", function(id){
		if(id =="find"){
	    	findInit();
	   }else if(id == "first"){
			pageAction(1, pageSize);
		}else if(id == "last"){
			pageAction(pageCount, pageSize);
		}else if(id == "next"){
			pageAction(pageNo + 1, pageSize);
		}else if(id == "pervious"){
			pageAction(pageNo -1, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,项目编号,项目名称,研发部门,测试阶段,&nbsp,计划开始时间,计划结束时间,&nbsp,状态,测试负责人,&nbsp;");
    pmGrid.setInitWidths("40,40,80,110,80,70,0,90,90,0,40,*,0");
    pmGrid.setColAlign("center,center,left,left,left,center,left,center,center,center,center,left,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("str,int,str,str,str,str,str,str,str,str,str,str,str");
	//pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,true,true,true,true,true,true,true,true,true,true,true,false");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr");
    pmGrid.attachEvent("OnCheck",doOnCheck);
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		$("reportTaskId").value = rowId;
		$("proNum_rep").value = pmGrid.cells(rowId,2).getValue();
		$("proName_rep").value = pmGrid.cells(rowId,3).getValue();
		if($("dateChk").value !=""){
			//alert("dateChk");
			loadDateLimit();
		}else{
			loadReport();
		}
		return true;
	}
    function doOnSelect(rowId,index){
		pmGrid.setSelectedRow(rowId);
	}
	function loadReport(){
		//alert($("reportTaskId").value);
		//alert($("repTemplet").value);
		var url = conextPath+"/frameset?__report=report/testReport/" + $("repTemplet").value + ".rptdesign&taskId="+$("reportTaskId").value;
		if($("dateChk").value !=""){
			if($("reportTaskId").value==""){
				hintMsg("请选择测试项目或测试任务");
				return;
			}
			if($("factStartDate").value==""){
				hintMsg($("cUMTxt").innerHTML);
				return ;
			}
			if(isWhitespace($("startDate").value)){
				hintMsg("启始日期不能为空");
				return ;
			}else if(isWhitespace($("endDate").value)){
				hintMsg("终止日期不能为空");
				return ;
			}
			var factStartDate = new Date($("factStartDate").value);
			var factEndDate = new Date($("factEndDate").value);
			var startDate = new Date($("startDate").value);
			var endDate = new Date($("endDate").value);
			if((startDate.getTime() - endDate.getTime())>0){
				hintMsg("启始日期不能在终止日期之后");
				return ;				 
			}	
			if((startDate.getTime() - factStartDate.getTime())<0){
				hintMsg("启始日期不在日期范围限制内");
				return ;				 
			}
			if((endDate.getTime() - factEndDate.getTime())>0){
				hintMsg("终止日期不在日期范围限制内");
				return ;				 
			}
			url = url +"&start_date="+$("startDate").value +"&end_date="+$("endDate").value;			
		}
		parent.mypmLayout.items[1].attachURL(url);
		parent.parameterW_ch.hide();
		pmGrid.cells($("reportTaskId").value, 0).setValue(false);		
		
	}
	function loadDateLimit(){
		var invMethod = "";
		if($("dateChk").value=="bugDate")
			invMethod = "getBugDateLimit";
		else if ($("dateChk").value=="exeCaseDate")
		   invMethod = "getExeCaseDateLimit";
		else 
		 invMethod = "getWriteCaseDateLimit";
		var url = conextPath+"/singleTestTask/singleTestTaskAction!"+invMethod+".action?dto.singleTest.taskId="+$("reportTaskId").value;
		var rest = dhtmlxAjax.postSync(url, "").xmlDoc.responseText.split("_");
		$("factStartDate").value= rest[0];
		$("factEndDate").value= rest[1];
		if(rest[0]!==""){
			$("cUMTxt").innerHTML = "从"+rest[0] +"到" + rest[1];
			$("endDate").value = rest[1];
			$("startDate").value = rest[0];
		}else{
			if("bugDate"==$("dateChk").value){
				$("cUMTxt").innerHTML = "当前项目无BUG数据";
			}else{
				$("cUMTxt").innerHTML = "当前项目无用例执行记录";
			}
		}
			
	}
	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount && pageSizec<1){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url = conextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action?dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		var userJson = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		pmGrid.clearAll();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum(pmGrid);
   		}

	}	


	function findInit(){
		fW_ch = initW_ch(fW_ch, "findDiv", true, 500, 135);
		fW_ch.setText("查询");
		fW_ch.show();
		fW_ch.bringToTop();
	}
	function findExe(){
		var url = conextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action";
		var userJson = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText.split("$");
		if(userJson[0]=="failed"){
			hintMsg("查询发重错误");
			return;
		}
		if(userJson[1] == ""){
			pmGrid.clearAll();
			setPageNoSizeCount(userJson[0]);
		}else{
	   		pmGrid.clearAll();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum(pmGrid);
	   		fW_ch.setModal(false);
	   		fW_ch.hide();
   		}
	}
	
	function reSetRepPra(){
		formReset('reportForm');
		var reportTaskId = $("reportTaskId").value;
		$("reportTaskId").value = "";
		if(reportTaskId!="")
			pmGrid.cells(reportTaskId, 0).setValue(false);
		$("cUMTxt").innerHTML = "";
	}