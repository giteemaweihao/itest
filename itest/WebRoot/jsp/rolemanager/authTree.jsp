<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib prefix="ww" uri="/webwork"%>
<%@ taglib prefix="c" uri="/WEB-INF/c.tld"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
	<script type="text/javascript">
		popContextMenuFlg=0;
		var custContextMenuFlg=0;
	</script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.css">
		<script  src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/dhtmlxTree/codebase/dhtmlxtree.js"></script>
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
		<title>权限查询</title>
	</head>
	<script type="text/javascript">
	popContextMenuFlg=0;
	</script>
	<body bgcolor="#ffffff" leftmargin="0" topmargin="3" marginwidth="0"  style="overflow-y:hidden;">
		<ww:hidden id="authTree" name="dto.authTree"></ww:hidden>
		<table width="100%">
		<tr>
		  <td>&nbsp;
		  </td>
		  <td>
		    <a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="javascript:parent.authTreeW_ch.setModal(false);parent.authTreeW_ch.hide();"
				style="margin-left: 6px;"><span> 返回</span> </a>
		   	<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="selNode()"
				style="margin-left: 6px;"><span> 确定</span> </a>
		  </td>
		  <td>&nbsp;
		  </td>
		  <td>&nbsp;
		  </td>
		  <td>
		  </td>
		</tr>
		</table>
		<div id="treeBox" style="overflow-y:hidden;width:100%;height:95%"></div> 
		<script>
		tree=new dhtmlXTreeObject("treeBox","100%","100%",0);					
		tree.setImagePath(conextPath+"/dhtmlx/dhtmlxTree/codebase/imgs/");
		tree.enableCheckBoxes(true);
		tree.enableThreeStateCheckboxes(true);
		tree.enableHighlighting(1);
		tree.setImageArrays("plus","plus2.gif","plus3.gif","plus4.gif","plus.gif","plus5.gif");
		tree.setImageArrays("minus","minus2.gif","minus3.gif","minus4.gif","minus.gif","minus5.gif");
		tree.setStdImages("book.gif","books_open.gif","books_close.gif");
	    tree.insertNewItem('0','-1','选择权限');
		var treeNodeArray = $("authTree").value.split(";");
		for(var i=0;i<treeNodeArray.length-1;i++){
			var nodeContext= treeNodeArray[i];
			var node = nodeContext.split(",");
			var parentId =node[0] ;
			var id  = node[1];
			var text = node[2];
			if(text.indexOf("#")>=0){ //带#的是占位菜单,暂时不用
				continue;
			}
			tree.insertNewItem(parentId,id,text);
		}
		parent.htmlTreeWin=window;
		function recorverChk(){
			if(parent.$("funIdsF").value!=""){
				var selNode = parent.$("funIdsF").value.split(" ");
				for(var j=0;j<selNode.length;j++ ){
					tree.setCheck(selNode[j],1);
				}		
			}		
		}
		function selNode(){
			var seledNodes= tree.getAllCheckedBranches().split(",");
			if(seledNodes==""){
				parent.authTreeW_ch.setModal(false);
				parent.authTreeW_ch.hide();
				return;
			}
			var selLeafNodeCount = 0;
			var selNadeName = "";
			var selLeafNode = "";
			for(var i=0; i<seledNodes.length; i++){
				if(seledNodes[i]=="-1")
					continue;
				if(tree.hasChildren(seledNodes[i])<1){
					selLeafNode+=" " +seledNodes[i];
					selNadeName+="," +tree.getItemText(seledNodes[i])+"("+tree.getItemText(tree.getParentId(seledNodes[i]))+")";
					selLeafNodeCount++;
				}
			}
			if(selLeafNodeCount>5){
				hintMsg("最多可选五个权限");
				return;
			}
			var funIdsF= selLeafNode.substring(1);
			parent.$("funIdsF").value=funIdsF;
			parent.$("funNamesF").value=selNadeName.substring(1);
			parent.authTreeW_ch.setModal(false);
			parent.authTreeW_ch.hide();
		}
		</script>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</body>
</html>
