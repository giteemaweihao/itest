<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>用户列表</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
	</HEAD>
	<BODY bgcolor="#ffffff">
		<table width="835" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top" width="100%">
					<div id="toolbarObj"></div>
				</td>
			</tr>
			<tr>
				<td valign="top" width="100%">
					<div id="gridbox"></div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}" />
	<script>
	var pmBar ;
	pageBreakUrl = conextPath+'/userManager/userManagerAction!findUsers.action?dto.isAjax=true'+"&dto.pageSize=" + pageSize;
	pmBar = new dhtmlXToolbarObject("toolbarObj");
	pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	pmBar.addButton("find",1 , "", "search.gif");
	pmBar.setItemToolTip("find", "查询");
	pmBar.addButton("reFreshP",2 , "", "page_refresh.gif");
	pmBar.setItemToolTip("reFreshP", "刷新页面");
	pmBar.addSeparator("userMaSp",3);
	pmBar.addButton("retMain",4 ,'返回', "");
	pmBar.addSeparator("broAuSp",5);
	pmBar.addButton("first",6 , "", "first.gif", "first.gif");
	pmBar.setItemToolTip("first", "第一页");
	pmBar.addButton("pervious",7, "", "pervious.gif", "pervious.gif");
	pmBar.setItemToolTip("pervious", "上一页");
	pmBar.addSlider("slider",8, 80, 1, 30, 1, "", "", "%v");
	pmBar.setItemToolTip("slider", "滚动条翻页");
	pmBar.addButton("next",9, "", "next.gif", "next.gif");
	pmBar.setItemToolTip("next", "下一页");
	pmBar.addButton("last",10, "", "last.gif", "last.gif");
	pmBar.setItemToolTip("last", "末页");
	pmBar.addInput("page",11, "", 25);
	pmBar.addText("pageMessage",12, "");
	pmBar.addText("pageSizeText",13, "每页");
	var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'));
	pmBar.addButtonSelect("pageP",14, "page", opts);
	pmBar.setItemToolTip("pageP", "每页记录数");
	pmBar.addText("pageSizeTextEnd",15, "条");
	pmBar.setListOptionSelected('pageP','id1');
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,真实姓名,员工编号,登录帐号,联系电话,办公电话,职务,状态,&nbsp;");
    pmGrid.setInitWidths("0,40,135,120,100,120,130,100,60,0");
    pmGrid.setColAlign("center,center,left,left,left,left,left,left,left,left");
    pmGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,int,str,str,str,str,str,str,str,str");
	pmGrid.enableAutoHeight(true, 500);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,false,true,true,true,true,true,true,true,true");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr");
	pmBar.attachEvent("onClick", function(id) {
		if(id =="retMain"){
			parent.broUW_ch.setModal(false);
			parent.broUW_ch.hide();
		}
	});
</script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/rolemanager/browUser.js"></script>
		<div id="findDiv"  class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findForm" name="findForm"
				namespace="/userManager" action="">
				<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="odd_mypm" >
						<td align="right" class="rightM_center">登录帐号:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="loginName_f" name="dto.user.loginName"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" class="rightM_center">真实姓名:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="realName_f" name="dto.user.name"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" class="rightM_center" style="border-right:0;">员工编号:</td>
						<td style="border-right:0;">
							<ww:textfield id="employeeId_f" name="dto.user.employeeId"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" style="border-right:0;" class="rightM_center">办公电话:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="officeTelno_f" name="dto.user.officeTel"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" class="rightM_center" style="border-right:0;">
							用户状态:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:select id="userStatus_f" name="dto.user.status" cssClass="text_c"
								list="#{-1:'所有',1:'活动',0:'禁用'}" headerValue="-1"
								cssStyle="width:120;">
							</ww:select>
						</td>
						<td align="right" class="rightM_center" style="border-right:0;">
							所属组:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<select id="groupIds_f" name="dto.user.groupIds" Class="text_c"
								style="width: 120;">
							<option value="-1">所有</option> 
							</select>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="dataM_left" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" align="right">
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="cuU_b"onclick="efind();"
								style="margin-left: 6px;"><span> 查询</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="formReset('findForm');"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr>
						<td colspan="4">&nbsp;</td>
					</tr>
				</table>
			</ww:form>
		</div>
		</div>
        <ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</BODY>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
</HTML>
