	
	function initCufmsW(){
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}

	function initW_ch(obj, divId, mode, w, h,wId){
		//importWinJs();
		if((typeof obj != "undefined") && !obj.isHidden()){
			obj.setDimension(w, h);
			obj.centerOnScreen();
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.show();
			obj.bringToTop();
			obj.centerOnScreen();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			if(typeof wId != "undefined" && wId!=""){
				obj = cufmsW.createWindow(wId, 110, 110, w, h);
			}else{
				obj = cufmsW.createWindow(divId, 110, 110, w, h);
			}
			if(divId != null&&divId !=""){
				obj.attachObject(divId, false);
			}
			obj.centerOnScreen();
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}    
	
	var sTime =0;
	function popSelWin(passValId,passNameId,dataId,title){
		if(passValId==tyId&&((new Date()).getTime()-sTime)<1000){
			parent.detlW_ch.setModal(false);
			popW_ch.setModal(true);
			popW_ch.show();
			popW_ch.bringToTop();
			return;
		}
		if(passValId!="chargeOwner"&&passValId!="assinOwnerId"&&passValId!="genePhaseId"&&passValId!="devOwnerId"&&passValId!="intercessOwnerId"&&passValId!="testOwnerId"&&passValId!="analyseOwnerId"&&passValId!="fixVer"&&passValId!="verifyVer"){
			var reportId = parent.pmGrid.cells(parent.pmGrid.getSelectedId(),20).getValue();
			if(myId!=reportId&&roleStr.indexOf("1")<0&&roleStr.indexOf("2")<0&&roleStr.indexOf("8")<0){
				return;
			}		
		}
		//如果是设置版本数据，在所有项目模式下，要判断要不要重加载版本数据
		if(passValId=="fixVer"||passValId=="verifyVer"||passValId=="bugReptVer"){
			if(parent.bugTaskId!= parent.pmGrid.cells(parent.pmGrid.getSelectedId(),21).getValue()||parent.$(dataId).value==""){
				var url = conextPath + "/testTaskManager/testTaskManagerAction!loadVerSel.action";
				var ajaxRest = postSub(url,"");	 
				if(ajaxRest=="failed"){
					hintMsg('加载版本信息失败');
					return;
				}else{
					parent.$(dataId).value = replaceAll(ajaxRest,"^","$");
				}				
			}
		}
		tyId = passValId;
		tpNa = passNameId;	
		sTime = (new Date()).getTime();
		if(typeof treeW_ch != "undefined"&&treeW_ch.isModal()){
			treeW_ch.setModal(false);
		}
		if(typeof popW_ch == "undefined"){
			popW_ch = initW_ch(popW_ch, "", true, 300, 230,'popW_ch');
			popGrid = popW_ch.attachGrid()
			popGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			popGrid.setHeader(",,单击选择");
			popGrid.setInitWidths("0,0,*");
	    	popGrid.setColAlign("left,left,left");
	    	popGrid.setColTypes("ch,ro,ro");
	   		popGrid.setColSorting("Str,int,str");
	   		popGrid.enableTooltips("false,false,true");
	    	popGrid.enableAutoHeight(true, 180);
	    	popGrid.enableRowsHover(true, "red");
	        popGrid.init();
	    	popGrid.setSkin("light");
		    popGrid.attachEvent("onRowSelect",function (rowId,index){
	   			$(tyId).value=rowId;
	    		if((tyId=="testOwnerId"||tyId=="analyseOwnerId"||tyId=="assinOwnerId"||tyId=="devOwnerId")&&$("currStateId").value!=$("initState").value){
	    			$("nextOwnerId").value=rowId;
	    		}	
	   			$(tpNa).value = popGrid.cells(rowId,2).getValue();
	   			if("bugFreqId"==tyId){
	   				if($(tpNa).value=="偶尔出现"){
	   					$("repropTd").style.display="";
						$("reprop").style.display="";	   				
	   				}else{
	   					$("repropTd").style.display="none";
						$("reprop").style.display="none";
						$("reproPersent").value="";
					}
	   			}else if("bugOccaId"==tyId){
	   				if( $(tpNa).value=="首次出现"){
	   					$("geneCauseTd").style.display="";
						$("geneCauseF").style.display="";   				
	   				}else{
	   					$("geneCauseTd").style.display="none";
						$("geneCauseF").style.display="none";	   				
	   				}
	   			}
	   			popW_ch.setModal(false);
				popW_ch.hide();
			});
		}
		if(typeof(parent.detlW_ch)!="undefined")
			parent.detlW_ch.setModal(false);
		popW_ch.setModal(true);
		popW_ch.setText(title);
		popGrid.clearAll();
		var selData = parent.$(dataId).value;
		if(selData!=""){
			selDatas = selData.split("$");
			for(var i = 0; i < selDatas.length; i++){
				var items = selDatas[i].split(";")
				popGrid.addRow2(items[0],"0,,"+items[1],i);
			}
			if($(tyId).value!=""&&selData.indexOf($(tyId).value)>=0){
				popGrid.setRowColor($(tyId).value, "#CCCCCC");
			}
		}
		popW_ch.show();
		popW_ch.bringToTop();
		parent.bugTaskId= parent.pmGrid.cells(parent.pmGrid.getSelectedId(),21).getValue();
		return;
	}

	function exist(id){
    	var testEle=$(id);
    	if(testEle){
    		return true
    	}else{
    		return false
    	}
   	}
	function styleChange(num){
	 	 	switch(num){
				case 1:
					$("baseInfoTab").className="jihuo";
					$("flowInfoTab").className="nojihuo";
					loadBaseInfo();
					break;
				case 2:
					$("baseInfoTab").className="nojihuo";
					$("flowInfoTab").className="jihuo";
					 loadFlowInfo();
					break;
	 		}
	 } 
	 function loadBaseInfo(){
	 	$("flowInfoDiv").style.display="none";
	 	$("baseInfoDiv").style.display="";
	 }

	 function loadFlowInfo(){
	 	if($("listStr").value==""){
			var url = conextPath + "/bugManager/bugManagerAction!loadHistory.action?dto.isAjax=true&dto.bug.bugId="+$("bugId").value;
			var ajaxRest = postSub(url,"");	 
			if(ajaxRest=="failed"){
				hintMsg('加载历史信息失败');
				return;
			}
			var data = ajaxRest.replace(/[\r\n]/g, "");
			$("toolbarObj1").style.width = $("gridbox1").style.width;
			$("listStr").value=data;
		    initFlwGrid(data);
		}
		$("baseInfoDiv").style.display="none";	 
	 	$("flowInfoDiv").style.display="";	
	}	
	function flwPageAction(pageNum){
		if(pageNum>flwPageCount){
			flwBar.setValue("page", flwPageCount);
			return ;
		}
		var url = conextPath + "/bugManager/bugManagerAction!loadHistory.action?dto.isAjax=true&dto.pageSize=20&dto.pageNo="+ pageNum+"&dto.bug.bugId="+$("bugId").value;
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="failed"){
			hintMsg('加载数据出错');
			return;
		}
		initFlwGrid(ajaxRest);
	}
	function initFlwGrid(ajaxRest){
	    if(ajaxRest != ""){
	    	ajaxRest = ajaxRest.replace(/[\r\n]/g, "");
		    var datas = ajaxRest.split("$");
		    if(datas[1] != ""){
		    	pmGrid.clearAll();
		    	jsons = eval("(" + datas[1] +")");
		    	pmGrid.parse(jsons, "json");
		    	setFlwRowNum(pmGrid);
		    	setFlwPageNoInfo(datas[0]);
		    }else{
		    	flwBar.disableItem("slider");
		    	setFlwPageNoInfo(datas[0]);
		    }
	    }	
	}
 	flwBar.attachEvent("onValueChange", function(id, pageNum) {
		flwPageAction(pageNum);
		return;
	});
	flwBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			if(value>flwPageCount){
				value=flwPageCount;
			}
			flwBar.setValue("page", value);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > flwPageCount){
			pageNoTemp = flwPageCount;
		}
		flwPageAction(pageNoTemp);
		return;
	});
	flwBar.attachEvent("onClick", function(id) {
		if(id == "first"){
			flwPageNo = 1;
			flwPageAction(flwPageNo);
		}else if(id == "last"){
			flwPageNo = flwPageCount;
			flwPageAction(flwPageNo);
		}else if(id == "next"){
			flwPageNo = flwPageNo +1
			flwPageAction(flwPageNo);
		}else if(id == "pervious"){
			flwPageNo = flwPageNo -1
			flwPageAction(flwPageNo);
		}
	});		
	
    function setFlwRowNum(){
		for(var i = 0; i < pmGrid.getRowsNum(); i++){	
			pmGrid.cells2(i,1).setValue((i + 1)+((parseInt(flwPageNo)-1)*20));
		}
    }
	function setFlwPageNoInfo(pageStr){
		var pageNoSizeCount = pageStr.split("/");
		flwPageNo = parseInt(pageNoSizeCount[0]);
		flwPageCount = parseInt(pageNoSizeCount[2]);
		flwBar.setValue("page", flwPageNo);
		if(flwPageCount==0){
			flwBar.setMaxValue("slider", 1, "");
		}else{
			flwBar.setMaxValue("slider", flwPageCount, "");
		}
		flwBar.setValue("slider", flwPageNo);
		if(flwPageNo == 1){
			flwBar.disableItem("first");
			flwBar.disableItem("pervious");
			if(flwPageCount <= 1){
				flwBar.disableItem("next");
				flwBar.disableItem("last");
				flwBar.disableItem("slider");
			}else{
				flwBar.enableItem("next");
				flwBar.enableItem("last");
				flwBar.enableItem("slider");
			}
		}else if(flwPageNo == flwPageCount&&flwPageCount>1){
			flwBar.enableItem("first");
			flwBar.enableItem("pervious");
			flwBar.disableItem("next");
			flwBar.disableItem("last");
		}else if(flwPageNo > 1 && flwPageNo < flwPageCount){
			flwBar.enableItem("first");
			flwBar.enableItem("pervious");
			flwBar.enableItem("next");
			flwBar.enableItem("last");
		}else{
			flwBar.disableItem("first");
			flwBar.disableItem("pervious");
			flwBar.disableItem("next");
			flwBar.disableItem("last");
			flwBar.disableItem("slider");
		}
		if(flwPageCount==0){
			flwBar.setItemText("pageMessage",  "无记录");
			flwBar.setValue("page", "");
		}else{
			flwBar.setItemText("pageMessage", "/ " + flwPageCount);
		}
	}
	



