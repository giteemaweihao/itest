<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="ww" uri="/webwork"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<HEAD>
		<TITLE>用户组管理</TITLE>
	<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css"></link>
	<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>		
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
	</HEAD>
	<BODY bgcolor="#ffffff" style="overflow-x:hidden;">
	
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<div id="toolbarObj" width="100%"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox"  width="100%"></div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}" />
	<script>
	ininPage("toolbarObj", "gridbox", 440);
	<pmTag:button page="userManager/group"  find="true" checkAll="false"/>
	</script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/userManager/group.js"></script>
	
		<div id="createD" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="createF" name="createF"
				namespace="/userManager"
				action="">
				<ww:hidden id="groupId" name="dto.group.id"></ww:hidden>
				<ww:hidden id="adminFlag" name="dto.group.adminFlag"></ww:hidden>
				<ww:hidden id="insertDate" name="dto.group.insertDate"></ww:hidden>
				<table id="createTable" class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0">
							<div id="cUMTxt" align="center" style="color: Blue; padding: 2px">
								&nbsp;
							</div>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" class="rightM_center" nowrap style="border-right:0;color: Red;width:50">
								组名:
						</td>
						<td colspan="3" style="border-right:0;width:100%" class="dataM_left">
							<ww:textfield id="name" name="dto.group.name" maxLength="16"
								cssStyle="width:100%;padding:2 0 0 4;" cssClass="text_c"
								></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" nowrap class="rightM_center" style="border-right:0;width:50">
								组员:
						</td>
						<td colspan="3" style="border-right:0;width:100%" class="dataM_left">
							<ww:hidden id="userIds" name="dto.group.userIds"></ww:hidden>
							<ww:textfield id="userName" name="userName" readonly="true"
								cssStyle="width:100%;padding:2 0 0 4;" cssClass="text_c" onfocus="popselWin('userIds','userName');"></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" nowrap class="rightM_center" style="border-right:0;width:50">
								备注:
						</td>
						<td colspan="3" style="border-right:0;width:100%" class="dataM_left" >
							<ww:textfield id="remark" name="dto.group.remark" maxLength="100"
								cssStyle="width:100%;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0;">&nbsp;
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" align="right">
						<a class="bluebtn" href="javascript:void(0);" id="retBtn"onclick="cuW_ch.setModal(false);cuW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
						<a class="bluebtn" href="javascript:void(0);" id="creat2_b"onclick="addUpSub('contF');"
							style="margin-left: 6px;display:none"><span> 确定并继续</span> </a>
						<a class="bluebtn" href="javascript:void(0);" id="creat_b"onclick="addUpSub();"
							style="margin-left: 6px;"><span> 确定</span> </a>
						<a class="bluebtn" href="javascript:void(0);" id="restBtn"onclick="$('userIds').value='';formReset('createF', 'name');"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="3" style="border-right:0;">&nbsp;
						</td>
					</tr>
				</table>
			</ww:form>
			</div>
		</div>
		<div id="findD" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findF" name="findF"
				namespace="/userManager"
				action="">
				<table  class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0">&nbsp;</td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" class="rightM_center" style="border-right:0;width:50">组名:</td>
						<td colspan="3" class="dataM_left"style="border-right:0" >
							<ww:textfield id="nameF" name="dto.group.name" cssClass="text_c"
								cssStyle="width:100%;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" class="rightM_center" style="border-right:0;width:50">组员:</td>
						<td colspan="3" class="dataM_left"style="border-right:0" >
							<ww:hidden id="userIdsF" name="dto.group.userIds"></ww:hidden>
							<ww:textfield id="userIdsNameF" name="userIdsNameF" cssClass="text_c" readonly="true"
								cssStyle="width:100%;padding:2 0 0 4;" onfocus="popselWin('userIdsF','userIdsNameF');"></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4">&nbsp;</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" align="right">
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="cuU_b"onclick="efind();"
								style="margin-left: 6px;"><span> 查询</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="$('userIdsF').value='';formReset('findF', 'nameF');"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan=4" >&nbsp;</td>
					</tr>
				</table>
			</ww:form>
			</div>
		</div>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
		<ww:include value="/jsp/common/selCompPerson.jsp"></ww:include>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	</body>
</html>
