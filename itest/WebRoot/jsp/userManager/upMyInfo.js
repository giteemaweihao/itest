	var currStatus;

	function addUpInitCB(id){
		$("cuU2_b").style.display="";
		$("cuU_b").style.display="";
		if(id=="new"){
			$("inVoldPwdR").style.display = "none";
			$("userId").value = "";
			$("groupNames").value = "";
			$("groupIds").value = "";
			$("cuU_b").value = " 确定 ";
			$("questionR").style.display = "";
			$("answerR").style.display = "";	
			$("cuU2_b").style.display = "";
			$("loginName").disabled=false;
		}else if(id=="update"){
			$("groupNames").value = "";
			$("groupIds").value = "";
			$("questionR").style.display = "none";
			$("answerR").style.display = "none";
			$("cuU2_b").style.display = "none";
			$("questionR").style.display = "";
			$("answerR").style.display = "";
			$("inVoldPwdR").style.display = "";
			$("cuU_b").value = " 修改 ";
			$("loginName").disabled=true;
		}	
		$("voldPwd").value= $("oldPwd").value;
		$("password").value= $("oldPwd").value;
		$("vpassword").value= $("oldPwd").value;
		adjustTable("createTab");
		return true;
	}
	function popMutlGridCB(){
		suW_ch = initW_ch(suW_ch, null, true, 220,200);
		if(typeof popGrid == "undefined"){
			popGrid = suW_ch.attachGrid();
			suW_ch.setText("选择组");
			popGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			popGrid.setHeader("选择,序号,组名");
			popGrid.setInitWidths("50,50,115");
	    	popGrid.setColAlign("center,left,left");
	    	popGrid.setColTypes("ch,ro,ro");
	   		popGrid.setColSorting("Str,int,str");
	   		popGrid.enableTooltips("false,false,true");
	        popGrid.init();
	    	popGrid.setSkin("light");		
		}
    	return popGrid;
	}
	var repChkCount=0,reName="0",reChk=0;
	function loginNameChk(){
		if(isWhitespace($("loginName").value)){
			$("cUMTxt").innerHTML="&nbsp;";
			return;
		}
		if(repChkCount==5)
			return;
		$("cuU2_b").style.display="none";
		$("cuU_b").style.display="none";
		var url = conextPath+"/commonAction!regisChk.action?dto.objName=User&dto.nameVal="+$("loginName").value;
		url+="&dto.namePropName=loginName&dto.idPropName=id&&dto.idPropVal="+$("userId").value;
		var ajaxRest = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
		repChkCount++;
		if(ajaxRest=="true"){
			$("cUMTxt").innerHTML = "所填登录帐号重复";
			reName="1";
			$("cuU2_b").style.display="";
			$("cuU_b").style.display="";
			return;
		}
		$("cUMTxt").innerHTML="&nbsp;";
		$("cuU2_b").style.display="";
		$("cuU_b").style.display="";
		reName="0";
	}
	var mailChkCount=0,reMail="0";
	function mailChk(){
		if(isWhitespace($("email").value)){
			$("cUMTxt").innerHTML="";
			return;
		}
		if(mailChkCount==5){
			return;
		}
		$("cuU2_b").style.display="none";
		$("cuU_b").style.display="none";
		var url = conextPath+"/commonAction!reNameChk.action?dto.objName=User&dto.nameVal="+$("email").value;
		url+="&dto.namePropName=email&dto.idPropName=id&&dto.idPropVal="+$("userId").value;
		var ajaxRest = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
		mailChkCount++;
		if(ajaxRest=="true"){
			$("cUMTxt").innerHTML = "所填电子信箱己被使用";
			reMail="1";
			return;
		}		
		reMail="0";
		$("cUMTxt").innerHTML="";
		if($("userId").value=="")
			$("cuU2_b").style.display="";
		$("cuU_b").style.display="";
	}
	function subMyInfo(){
		if(subCheck()){
			var url= conextPath+"/userManager/userManagerAction!updateMyInfo.action";
			var ajaxResut = dhtmlxAjax.postSync(url, "createForm").xmlDoc.responseText;
			if(ajaxResut.indexOf("success")==0){
				parent.myInfoW_ch.setModal(false);
				parent.myInfoW_ch.hide();
			}else if(ajaxResut.indexOf("customMsg")==0){
				$("cUMTxt").innerHTML = "原密码不正确";
			}
			else{
				$("cUMTxt").innerHTML = "保存数据发错误";
			}
		}
	}
	function subCheck(){
		if(isWhitespace($("loginName").value)){
			$("cUMTxt").innerHTML = "请填写登录帐号";
			$("loginName").focus();
			return false;
		}else if(isWhitespace($("password").value)){
			$("password").focus();
			$("cUMTxt").innerHTML = "请输入登录密码";
			return false;
		}else if(!checkIsOverLong($("password").value,5)){
			$("password").focus();
			$("cUMTxt").innerHTML = "密码不能少于6位";
			return false;
		}else if($("password").value != $("vpassword").value){
			$("vpassword").focus();
			$("cUMTxt").innerHTML = "确认密码不正确";
			return false;
		}else if(isWhitespace($("name").value)){
			$("cUMTxt").innerHTML = "请输入真实姓名";
			$("name").focus();
			return false;
		}else if(isWhitespace($("email").value)){
			$("cUMTxt").innerHTML = "请输入邮箱";
			$("email").focus();
			return false;
		}else if(!isEmailAddress($("email").value)){
			$("cUMTxt").innerHTML = "管理员邮箱格式不正确";
			$("email").focus();
			return false;		
		}else if($("userId").value != ""&&isWhitespace($("voldPwd").value)){
			$("voldPwd").focus();
			$("cUMTxt").innerHTML = "请输入原密码";
			return false;			
		}else if(checkIsOverLong($("employeeId").value,20)){
			$("cUMTxt").innerHTML = "员工编号不能超过20位";
			$("employeeId").focus();
			return false;		
		}else if(checkIsOverLong($("officeTel").value,20)){
			$("cUMTxt").innerHTML = "办公电话不能超过20位";
			$("officeTel").focus();
			return false;			
		}else if(checkIsOverLong($("tel").value,20)){
			$("cUMTxt").innerHTML = "联系电话不能超过20位";
			$("tel").focus();
			return false;			
		}else if(checkIsOverLong($("email").value,32)){
			$("cUMTxt").innerHTML = "电子信箱不能超过32位";
			$("email").focus();
			return false;				
		}else if(checkIsOverLong($("question").value,20)){
			$("cUMTxt").innerHTML = "密码问题不能超过20位";
			$("question").focus();
			return false;			
		}else if(checkIsOverLong($("answer").value,50)){
			$("cUMTxt").innerHTML = "密码答案不能超过50位";
			$("answer").focus();
			return false;			
		}else if(reMail=="1"){
			$("cUMTxt").innerHTML = "所填电子信箱己被使用";
			return false;
		}else if(reName=="1"){
			$("cUMTxt").innerHTML = "所填管理员登录帐号己被注册";
			return  false;
		}else if(!speCharChk("createForm")){
			$("cUMTxt").innerHTML = "不能含特殊字符";
			return  false;		
		}
		return true;
	}

	function chgPwd(){
		var oldPwd=$('oldPwd').value;
		if(oldPwd!=$("password").value){
			$('voldPwd').value='';
			$("chgPwdFlg").value=$("userId").value;
		}else{
			$('voldPwd').value=oldPwd;
		}
	} 
	function popMutlGrid(url,passValId,passNameId,callBack){
		if(""==url){
			hintMsg("您没有操作权限");
			return;
		}
		if(suW_ch.button("dock") == null){
			suW_ch.addUserButton("dock", 0, "选择", "dock");
			suW_ch.button("dock").attachEvent("onClick", function(){
			var Ids = "";
			var Names = "";
			var allItems = popGrid.getAllItemIds();
			var items = allItems.split(',');
			for(var i = 0; i < items.length; i++){
				if(items[i] != "" && popGrid.cells(items[i], 0).getValue() == 1){
					if (Ids == ""){
						Ids = items[i];
						Names =  popGrid.cells(items[i], 2).getValue();
					}else{
						Ids += "," + items[i];
						Names += "," + popGrid.cells(items[i], 2).getValue()
					}
				}
			}
			$(passNameId).value = Names;
			$(passValId).value = Ids ;
			suW_ch.hide();
			suW_ch.setModal(false);
			});
		}
		popGrid.attachEvent("onCheckbox",function(rowId,cellInd,state){
			popGrid.cells(rowId,0).setValue(true);
			return true;
		});
    	var ajaxResut = dhtmlxAjax.postSync(url, "");
    	popGrid.clearAll();
   		var jsonText = ajaxResut.xmlDoc.responseText;
    	if(jsonText != ""){
    		jsonText = jsonText.replace(/[\r\n]/g, "");
   			popGrid.parse(eval("(" + jsonText +")"), "json");
   		}
   		suW_ch.bringToTop();
	}
	function isEmailAddress(value, allowEmpty){
	  var strText = new String(value);
	  if (allowEmpty == true && value.length == 0) {
	    return true;
	  }
	  var strPattern = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	  var strPatt = /[^a-zA-Z0-9-@\._]/;
	  if (strText.match(strPatt)) {
	     return false;
	  }
	  aryResult = strText.match(strPattern);
	  if(aryResult == null) {
	  return false;
	  } else {
	  return true;
	 }
	}

  function speCharChk(formId){
	var form = document.getElementById(formId);
    var elements = form.elements;  
    for (i = 0; i < elements.length; ++i) {
      var element = elements[i];
      if(element.type == "text" || element.type == "textarea" || element.type == "hidden"){
      	if(element.id=="groupNames"||element.id=="groupIds"||element.id=="voldPwd"||element.id=="password"||element.id=="vpassword"||element.id=="insertDate")
      		continue;
      	if(includeSpeChar(element.value)){
      		return false;
      	}
      }
     }
    return true;
  }
  function includeSpeChar(StrVal){
  	var speStr="~ ` ! # $ % ^ & * ( ) [ ] { } ; ' : \" , ， < >";
  	var speStrArr = speStr.split(" ");
  	if(typeof(StrVal)=="undefined"||isWhitespace(StrVal))
  		return false;
  	for(var i=0; i<speStrArr.length; i++){
  		if(StrVal.indexOf(speStrArr[i])>=0){
  			return true;
  		}
  	}
  	return false;
  }