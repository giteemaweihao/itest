<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>管理帐户</TITLE>
	<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css"></link>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>		
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
	</HEAD>
	<BODY bgcolor="#ffffff">
	
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<div id="toolbarObj" width="100%"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox"  width="100%"></div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}" />
	<script>
	ininPage("toolbarObj", "gridbox", 740);
	<pmTag:button page="userManager/company"  find="true" checkAll="false"/>
	</script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/userManager/company.js"></script>
		<div id="createD" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="createF" name="createF"
				namespace="/userManager"
				action="userManagerAction!companyMaintence.action">
				<ww:hidden id="companyId" name="dto.company.id"
					value="${dto.user.companyId}"></ww:hidden>
				<ww:hidden id="userId" name="dto.user.id" value="${dto.user.id}"></ww:hidden>
				<ww:hidden id="status" name="dto.company.status" value="${dto.company.status}"></ww:hidden>
				<ww:hidden id="regisIp" name="dto.company.regisIp" value="${dto.company.status}"></ww:hidden>
				
				<table id="createTable" class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td colspan="4" style="border-right:0">
							<div id="msgD" align="center" style="color: Blue; padding: 2px">
								&nbsp;
							</div>
						</td>
					</tr>
					<tr >
						<td align="right" class="rightM_center" style="border-right:0;color: Red;">
								公司名称:
						</td>
						<td colspan="4" style="border-right:0;" class="dataM_left">
							<ww:textfield id="name" name="dto.company.name"
								cssStyle="width:100%;padding:2 0 0 4;" cssClass="text_c"
								onblur="javascript:if(!isWhitespace(this.value)){$('msgD').innerHTML='&nbsp;'};"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right"  class="rightM_center"style="border-right:0;color: Red;">
								公司类型:
						</td>
						<td style="border-right:0;" class="dataM_left">
						<ww:select id="companyType" name="dto.company.type" cssClass="text_c"
						list="#{0:'选择',1:'互联网',2:'计算机软件',3:'系统集成',4:'其它'}" headerValue="0" cssStyle="width:120;">
						</ww:select>
						</td>
						<td align="right" class="rightM_center" style="border-right:0;color: Red;" >
								公司规模:
						</td>
						<td style="border-right:0;" class="dataM_left">
						<ww:select id="companySize" name="dto.company.compSize" cssClass="text_c"
						list="#{0:'选择',1:'1-20人',2:'21～50人',3:'51～100人',4:'101～200人',5:'200～300人',6:'301～500人',7:'500～1000人',8:'1000人以上'}" headerValue="0" cssStyle="width:120;">
						</ww:select>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0;color: Red;">
							公司简介:
						</td>
						<td colspan="3" style="border-right:0;" class="dataM_left">
							<ww:textfield id="remark" name="dto.company.remark" cssClass="text_c"
								 cssStyle="width:100%;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0;color: Red;">
							公司地址:
						</td>
						<td colspan="3" style="border-right:0;" class="dataM_left">
							<ww:textfield id="address" name="dto.company.address" cssClass="text_c"
								 cssStyle="width:100%;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0;color: Red;">
							公司电话:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="telephone" name="dto.company.telephone" cssClass="text_c"
								cssStyle="width:120;padding:2 0 0 4;"></ww:textfield>
						</td>
						<td align="right" class="rightM_center" style="border-right:0;">
							公司传真:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="fax" name="dto.company.fax" cssClass="text_c"
								cssStyle="width:120;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0;">
							公司网址:
						</td>
						<td colspan="3" style="border-right:0;" class="dataM_left">
							<ww:textfield id="url" name="dto.company.url" cssClass="text_c"
								cssStyle="width:100%;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0;color: Red;">
							管理员姓名:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="owner" name="dto.company.owner"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"
								onblur="javascript:if(!isWhitespace(this.value)){$('msgD').innerHTML='&nbsp;'};"></ww:textfield>
						</td>
						<td align="right" class="rightM_center" style="border-right:0;color: Red;">
							管理员帐号:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="loginName" name="dto.user.loginName" cssClass="text_c"
								cssStyle="width:120;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr id="inVoldPwdR" style="display: none">
						<td align="right" class="rightM_center" style="border-right:0;color: Red;">
								原密码:
						</td>
						<td colspan="3" style="border-right:0;" class="dataM_left">
							<ww:password id="voldPwd" name="inVoldPwd" cssClass="text_c"
								cssStyle="width:100%;padding:2 0 0 4;"
								onblur="javascript:if(!isWhitespace(this.value)){$('msgD').innerHTML='&nbsp;'};"></ww:password>
						</td>
					</tr>
					<tr id="pwdR" style="display: none">
						<td align="right" class="rightM_center"  style="border-right:0;color: Red;">
								登录密码:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:password id="password" name="dto.user.password" cssClass="text_c"
								cssStyle="width:120;padding:2 0 0 4;"></ww:password>
						</td>
						<td align="right" class="rightM_center" style="border-right:0;color: Red;">
								确认密码:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:password id="vpassword" name="vpassword" cssClass="text_c"
								cssStyle="width:120;padding:2 0 0 4;"></ww:password>
						</td>
					</tr>
					<tr>
						<td align="right" class="rightM_center" style="border-right:0;">
							管理员电话:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="ownerTel" name="dto.company.ownerTel" cssClass="text_c"
								cssStyle="width:120;padding:2 0 0 4;"></ww:textfield>
						</td>
						<td align="right" class="rightM_center" style="border-right:0;color: Red;">
							管理员邮箱:
						</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="ownereMail" name="dto.company.ownereMail" cssClass="text_c"
								cssStyle="width:120;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr id="questionR" style="display: none" >
						<td align="right" class="rightM_center" style="border-right:">
							找回密码问题:
						</td>
						<td colspan="3" style="border-right:0;" class="dataM_left">
							<ww:textarea id="question" name="dto.user.question" cssClass="text_c"
								cssStyle="width:100%;padding:2 0 0 4;" rows="2"></ww:textarea>
						</td>
					</tr>
					<tr id="answerR" style="display: none">
						<td align="right" class="rightM_center" style="border-right:0;">
							找回密码答案:
						</td>
						<td colspan="3" style="border-right:0;" class="dataM_left">
							<ww:textarea id="answer" name="dto.user.answer" cssClass="text_c"
								cssStyle="width:100%;padding:2 0 0 4;" rows="2"></ww:textarea>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="border-right:0;">&nbsp;
						</td>
					</tr>
					<tr>
						<td colspan="4" align="right">
						<a class="bluebtn" href="javascript:void(0);" id="retBtn"onclick="cuW_ch.setModal(false);cuW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
						<a class="bluebtn" href="javascript:void(0);" id="creat_b"onclick="checkCreateF();"
							style="margin-left: 6px;"><span> 确定</span> </a>
						<a class="bluebtn" href="javascript:void(0);" id="restBtn"onclick="formReset('createF', 'name');"
							style="margin-left: 6px;"><span> 重置</span> </a>
						
						</td>
					</tr>
					<tr>
						<td colspan="4" style="border-right:0;">&nbsp;
						</td>
					</tr>
				</table>
			</ww:form>
			</div>
		</div>
		<div id="findD" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findF" name="findF"
				namespace="/userManager"
				action="userManagerAction!adminList.action">
				<table  class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0">&nbsp;</td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" class="rightM_center" style="border-right:0">公司名称:</td>
						<td colspan="3" class="dataM_left"style="border-right:0" >
							<ww:textfield id="name_f" name="dto.company.name" cssClass="text_c"
								cssStyle="width:100%;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" class="rightM_center" style="border-right:0">
							管理员姓名:
						</td>
						<td class="dataM_left" style="border-right:0">
							<ww:textfield id="owner_f" name="dto.company.owner"  cssClass="text_c"
								cssStyle="width:120;padding:2 0 0 4;"></ww:textfield>
						</td>
						<td align="right" class="rightM_center" style="border-right:0">
							管理员帐号:
						</td>
						<td class="dataM_left" style="border-right:0">
							<ww:textfield id="loginName_f" name="dto.company.loginName"  cssClass="text_c"
								cssStyle="width:120;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" class="rightM_center" style="border-right:0">
							公司类型:
						</td>
						<td class="dataM_left" style="border-right:0">
						<ww:select id="companyType_f" name="dto.company.type"   cssClass="text_c"
						list="#{0:'选择',1:'互联网',2:'计算机软件',3:'系统集成',4:'其它'}" headerValue="0" cssStyle="width:120;">
						</ww:select>

						</td>
						<td align="right" class="rightM_center" style="border-right:0">
							公司规模:
						</td>
						<td class="dataM_left" style="border-right:0">
						<ww:select id="companySize_f" name="dto.company.companySize"  cssClass="text_c" 
						list="#{0:'选择',1:'1-20人',2:'21～50人',3:'51～100人',4:'101～200人',5:'200～300人',6:'301～500人',7:'500～1000人',8:'1000人以上'}" headerValue="0" cssStyle="width:120;">
						</ww:select>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4">&nbsp;</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" align="right">
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="cuU_b"onclick="efind();"
								style="margin-left: 6px;"><span> 查询</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="formReset('findF', 'name_f');"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan=4" >&nbsp;</td>
					</tr>
				</table>
			</ww:form>
			</div>
		</div>
		<div id="chgStatus" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<table  class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
				<tr class="ev_mypm">
					<td align="center"  style="color: Blue;" width="125" >
						<div id="cmText" align="center">
							&nbsp;
						</div>
					</td>
				</tr>
				<tr class="odd_mypm">
				 <td  align="center" style="color: Blue;"> 请选择要变更的状态
				 </td>
				</tr>
				<tr class="ev_mypm">
					<td  align="center" >
						<ww:select id="chgeStatus" name="dto.chgeStatus"  theme="simple"  cssClass="text_c"
						list="#{-2:'选择',-1:'禁用',0:'试用中',1:'正常使用中',2:'到期未续费 可用',3:'到期未续费 禁用'}" headerValue="-2" cssStyle="width:110;">
						</ww:select>
					</td>
				</tr>
				<tr class="odd_mypm">
					<td>&nbsp;</td>
				</tr>
				<tr class="ev_mypm">
					<td  align="center" >
						<a class="bluebtn" href="javascript:void(0);" id="cCancel"onclick="javascript:cW_ch.hide();cW_ch.setModal(false);"
							style="margin-left: 6px;"><span> 取消</span> </a>
						<a class="bluebtn" href="javascript:void(0);" id="cOkBtn"onclick="eChgSts();"
							style="margin-left: 6px;"><span> 确定</span> </a>
					</td>
				</tr>
			</table>
			</div>
		</div>
	</BODY>
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
</HTML>
