<%@ page language="java" pageEncoding="UTF-8"%>
<%@taglib prefix="ww" uri="/webwork"%>

<html> 
	<head>
	<script type="text/javascript">
		popContextMenuFlg=0;var custContextMenuFlg=0;
		var validCodeFlg = "";
		loginPage = true;
	</script>
		<title>MYPM项目管理平台</title>
		<link rel="shortcut icon"  type="image/x-icon" href="<%=request.getContextPath()%>/images/go.gif" />
		<link rel="Bookmark" type="image/x-icon"  href="<%=request.getContextPath()%>/images/go.gif" />
		<META content=MYPM项目管理平台 name=description>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/css/page.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<noscript><iframe src=*.html></iframe></noscript>
	<script type="text/javascript">
		importKeyCtrlJs(); haveLoadWin="true";popContextMenuFlg=0;
	</script>
	</head>
	<body oncontextmenu="return false" style="text-align: center">
		<div style="width:100%;height:150px;"></div>
		<div style="margin:0 auto;text-align:center;width:685px;height:260px;background:url(<%=request.getContextPath()%>/jsp/common/images/login_bj.gif);"></div>
		<div align="center" style="margin-top:-170px;">
		<div id="loginDiv" class="gridbox_light" style="width:450px;text-align:center;border:0px;">
			<div style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="loginForm" name="loginForm" namespace="/userManager" action="" >
				<input type="hidden" id="mailBugId" name="mailBugId" value="<%=request.getParameter("mailBugId")%>"/>
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr style="height:30px;">
						<td class="leftM_center" style="color:red;padding-left:60px;border-right:0px;">登录名:</td>
						<td style="border-right:0px;"><input name="dto.user.loginName" id="loginlName" type="text" class="text_c" style="width:170px;" onkeydown="enter(this,'autoFlg');" tabindex="1" /></td>
						<td width="130"><input id="isCookieLoginId" name ="saveLoginName" type="checkbox" value="1" onclick="reSetLogin(this)" /><label style="font-family:Tahoma;font-size:12px;" for="isCookieLoginId">下次自动登录</label></td>
					</tr>
					<tr id="tr_nbsp" style="height:20px;display:none;"><td colspan="3">&nbsp;</td></tr>
					<tr style="height:30px;">
						<td class="leftM_center" style="color:red;padding-left:60px;border-right:0px;">密&nbsp;&nbsp;码:</td><td style="border-right:0px;"><input name="dto.user.password" id="loginPwd" type="password" autocomplete="off" class="text_c" style="width:170px;" onkeydown="enter(this,'autoFlg');" tabindex="2" /></td><td>&nbsp;</td>
					</tr>
					<tr id="tr_ValiCode" style="height:30px;display:none">
						<td class="leftM_center" style="color:red;padding-left:60px;border-right:0px;" id="viewCodeTd">验证码:</td>
						<td style="border-right:0px;">
							<input type="text" name="dto.viewCode" id="viewCode" maxlength="4" class="text_c" style="width:170px;" onkeydown="enter(this,'autoFlg');" tabindex="3" />
							<input type="hidden" name="isCheckViewCode" id="isCheckViewCode" value="0" />
						<td>&nbsp;<img border="0" id="validCodeImage" name="viewCodeimg" /></td>
					</tr>
					<tr><td></td><td colspan="2"><div id="loginMessage" style="font-family: Tahoma;font-weight: bold;font-size:12px;color:red;text-align:left;">&nbsp;</div></td></tr>
				</table>
				</ww:form>
				<table  border="0" width="100%">
					<tr>
						<td align="center">
							<img id="loginButton" src="<%=request.getContextPath()%>/jsp/common/images/login/login_s.gif" style="cursor:pointer;" onclick="loginCheck('autoFlg');" onkeydown="enter(this,'autoFlg');" tabindex="4" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<img id="resetButton" src="<%=request.getContextPath()%>/jsp/common/images/login/login_r.gif" style="cursor:pointer;" onclick="formReset('loginForm', 'loginlName');" />&nbsp;
						</td>
					</tr>
					<tr>
					 <td>&nbsp;</td>
					</tr>
					<tr>
					 <td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style=";font-family: Tahoma;font-weight: bold;font-size:12px;color:blue;text-align:left;"> 
							 推荐Firefox3+,Chrome4+,支持IE7+,分辨率1024×768+<a href="javascript:void(0);" id="trLink" onclick="viewGuid()" style="margin-left: 6px;" >MYPM简易向导</a>						 
						</td>
					</tr>
				</table>
			</div>
		</div>
		</div>	
	<script type="text/javascript">
		var mailBugId = "<%=request.getParameter("mailBugId")%>";
		if(mailBugId=="null"){
			$("mailBugId").value="";
		}
		if(_isIE)$("loginMessage").innerHTML = "推荐您使用Firefox浏览器登录";
		if(!_isFF&&!_isIE){
		
		}
		importLoginJs();
		$("loginlName").focus();
		var loginId = getCookie("loginId");
		if(typeof(loginId) !="undefined" && loginId != ""&&loginId!="undefined"){
			$("loginlName").value = loginId;
			$("isCookieLoginId").checked=true;
			$("loginPwd").value = getCookie("attachInfo");
			$("loginMessage").innerHTML="自动登录中...";
			$("loginlName").readonly="true";
			$("loginPwd").readonly="true";
			loginCheck("autoFlg");
			$("loginlName").readonly="false";
			$("loginPwd").readonly="false";			
		}
		if("true"!=validCodeFlg){
			$("tr_ValiCode").style.display="none";
			$("tr_nbsp").style.display="";
		}	
	 var tutoriaW_ch,proW_ch;	
	function viewGuid(){
		tutoriaW_ch = initW_ch(tutoriaW_ch, "", false, 650, 450,'tutoriaW_ch');
		tutoriaW_ch.attachURL(conextPath+"/tutoria.html");
		tutoriaW_ch.setText("MYPM简易向导");	
	    tutoriaW_ch.show();
	    tutoriaW_ch.bringToTop();
	    tutoriaW_ch.setModal(false);
	} 			
	</script>	
		<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/userManager/login.js"></script>
    	<script language="javascript" type="text/javascript" src="http://js.users.51.la/3600188.js"></script>
		<noscript>
			<a href="http://www.51.la/?3600188" target="_blank"><img
					alt="&#x6211;&#x8981;&#x5566;&#x514D;&#x8D39;&#x7EDF;&#x8BA1;"
					src=" http://img.users.51.la/3600188.asp" style="border: none" />
			</a>
		</noscript>					
	</body>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.css">	
   	<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/calendar/codebase/dhtmlxcalendar.js"></script>
</html>
