	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	//最后一隐藏列为是否发布的标识,和模块 id
    pmGrid.setHeader("&nbsp;,编号,用例描述,&nbsp;,&nbsp;,最新状态,类别,优先级,最近处理人,&nbsp;,&nbsp;,编写人,成本,项目名称,编写日期,附件,&nbsp;,&nbsp;");
    pmGrid.setInitWidths("25,80,*,0,0,70,60,80,100,0,0,100,40,120,120,40,0,0");
    pmGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
    pmGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,int,str,str,str,st,str,str,str,str,str,str,str,str,str,str,str,str");
    pmGrid.enableTooltips("false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,false,false,false");
    pmGrid.setSkin("light");
    pmGrid.enableAutoHeight(true, 700);
    colTypeReset();
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
	initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");
    pmGrid.attachEvent("OnCheck",doOnCheck);
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    var chkAllBug=0;
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		if(!state&&chkAllBug==1){
			chkAllBug=0;
			pmBar.setItemImage("allChk", "ch_false.gif");
			pmBar.setItemToolTip("allChk", "全选");
		}else if(state&&chkAllBug==0){
		    var currChk= true;
			for(var i = 0; i < pmGrid.getRowsNum(); i++){
				if(pmGrid.cells(pmGrid.getRowId(i), 0).getValue()==0){
					currChk = false;
				}
			}
			if(currChk){				
				chkAllBug=1;
				pmBar.setItemImage("allChk", "ch_true.gif");
				pmBar.setItemToolTip("allChk", "取消全选");
			}			
		}
		return true;
	}
    function doOnSelect(rowId,index){
		pmGrid.setSelectedRow(rowId);
	}
	function colTypeReset(){
		pmGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		pmGrid.setColTypes("ch,ro,link,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
	}
	loadLink();
	function loadLink(){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			//pmGrid.cells2(i,1).cell.innerHTML="<a href='javascript:relaBug()' title=\"关联BUG\">"+getTttle2(i,1)+"</a>";
			if(getTttle2(i,2).indexOf("<a href")<0){
				pmGrid.cells2(i,2).cell.innerHTML="<a href='javascript:addUpInit(\"detal\")' title='查看明细-------"+getTttle2(i,2)+"'>"+getTttle2(i,2)+"</a>";
			}
			if(getTttle2(i,15)!=""&&getTttle2(i,15).indexOf("<img src")<0){
				pmGrid.cells2(i,15).cell.innerHTML="<img src='"+conextPath+"/images/button/attach.gif' alt='附件'  title='打开附件' onclick=\"openAtta('"+getTttle2(i,15)+"')\"/>";
			}
			
		}
		sw2Link();
	}
	function chkAllReset(){
		if(chkAllBug==0){
			for(var i = 0; i < pmGrid.getRowsNum(); i++){
				pmGrid.cells(pmGrid.getRowId(i), 0).setValue(true);
			}		
			chkAllBug=1;
			pmBar.setItemImage("all", "ch_true.gif");
			pmBar.setItemToolTip("all", "取消全选");
		}else{
			for(var i = 0; i < pmGrid.getRowsNum(); i++){
				pmGrid.cells(pmGrid.getRowId(i), 0).setValue(false);
			}		
			chkAllBug=0;
			pmBar.setItemImage("all", "ch_false.gif");
			pmBar.setItemToolTip("all", "全选");
		}
	}
	function getChecked(){
		selectItems = "";
		var allItems = pmGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			if(items[i] != "" && pmGrid.cells(items[i], 0).getValue() == 1){
				if (selectItems == ""){
					selectItems = items[i];
				}else{
					selectItems += "," + items[i];
				}
			}
		}
		return selectItems;
	}