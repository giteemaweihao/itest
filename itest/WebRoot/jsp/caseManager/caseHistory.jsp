<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<html>
	<head>
		<title>用例历史</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
	</head>
	<body bgcolor="#ffffff">
		<input type="hidden" id="listStr" name="listStr"value="${dto.listStr}" />
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<div id="toolbarObj"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox"></div>
				</td>
			</tr>
		</table>
	<script type="text/javascript">
	pageBreakUrl = conextPath+'/caseManager/caseManagerAction!viewCaseHistory.action';
	var pageSizec = 0;
	var pmBar = new dhtmlXToolbarObject("toolbarObj");
	pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	pmBar.addButton("reFreshP",0 , "", "page_refresh.gif");
	pmBar.setItemToolTip("reFreshP", "刷新页面");
	pmBar.addButton("first",2 , "", "first.gif", "first.gif");
	pmBar.setItemToolTip("first", "第一页");
	pmBar.addButton("pervious",3, "", "pervious.gif", "pervious.gif");
	pmBar.setItemToolTip("pervious", "上一页");
	pmBar.addSlider("slider",4, 80, 1, 30, 1, "", "", "%v");
	pmBar.setItemToolTip("slider", "滚动条翻页");
	pmBar.addButton("next",5, "", "next.gif", "next.gif");
	pmBar.setItemToolTip("next", "下一页");
	pmBar.addButton("last",6, "", "last.gif", "last.gif");
	pmBar.setItemToolTip("last", "末页");
	pmBar.addInput("page",7, "", 25);
	pmBar.addText("pageMessage",8, "");
	pmBar.addText("pageSizeText",9, "每页");
	var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'));
	pmBar.addButtonSelect("pageP",10, "page", opts);
	pmBar.setItemToolTip("pageP", "每页记录数");
	pmBar.addText("pageSizeTextEnd",11, "条");
	pmBar.setListOptionSelected('pageP','id1');
 
	pmBar.attachEvent("onClick", function(id) {
		if(id =="reFreshP"){
	   pageAction(pageNo, pageSize);
		}
	});
	
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	//最后一隐藏列为是否发布的标识,和模块 id
    pmGrid.setHeader("&nbsp;,&nbsp;,处理人,操作类型,用例状态,版本,处理日期,备注");
    pmGrid.setInitWidths("0,0,100,80,80,60,100,100");
    pmGrid.setColAlign("center,left,left,left,left,left,left,left");
    pmGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,int,str,str,str,int,str,str");
    pmGrid.enableTooltips("false,true,true,true,true,true,true,true");
    pmGrid.setSkin("light");
    pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
	initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");
	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var purl = pageBreakUrl + "?dto.isAjax=true&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		purl+="&dto.testCaseInfo.testCaseId="+"${dto.testCaseInfo.testCaseId}"
		var ajaxRest = postSub(purl,"");
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
			return;
		}else{
			pmGrid.clearAll();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		setPageNoSizeCount(userJson[0]);
   		}
   		return;
	}
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id == "first"){
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "last"){
			pageNo = pageCount;
			pageAction(pageNo, pageSize);
		}else if(id == "next"){
			pageNo = pageNo +1
			pageAction(pageNo, pageSize);
		}else if(id == "pervious"){
			pageNo = pageNo -1
			pageAction(pageNo, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});	
	</script>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</BODY>
</HTML>
