﻿	function loadCase(id){
		$("currNodeIdF").value=id
		var url=conextPath+"/caseManager/caseManagerAction!loadCase.action?dto.isAjax=true";
		var ajaxResut;
		//if($("remQuery").checked==false){
		//	ajaxResut =  postSub(url,"");
			//pfromReset();
		//}else{
			ajaxResut = postSub(url,"findForm");
			//cp2Pform();
		//}
		try{
			pmBar.disableItem("adtInit");
		}catch(err){}
		$("listStr").value=ajaxResut;
		if(typeof dW_ch != "undefined" && !dW_ch.isHidden()){
			dW_ch.hide();
			dW_ch.setModal(false);
		}
		if(typeof cW_ch != "undefined" && !cW_ch.isHidden()){
			cW_ch.hide();
			cW_ch.setModal(false);
		}
		if(typeof mW_ch != "undefined" && !mW_ch.isHidden()){
			mW_ch.hide();
			mW_ch.setModal(false);
		}
		if(typeof ptW_ch != "undefined" && !ptW_ch.isHidden()){
			ptW_ch.hide();
			ptW_ch.setModal(false);
		}	
		if(typeof cuW_ch != "undefined" && !cuW_ch.isHidden()){
			cuW_ch.hide();
			cuW_ch.setModal(false);
		}				
	    if(ajaxResut!="failed"){
	    	pmGrid.clearAll();
	    	 colTypeReset();
			initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");
			loadLink();
	    }
	    url=conextPath+"/caseManager/caseManagerAction!getCaseStatInfo.action?dto.currNodeId="+id;
	    ajaxResut =  postSub(url,"");
	    $("countStr").innerHTML =  ajaxResut;
	    return;			
	}
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	function quickQuery(){
		var url =  conextPath+"/caseManager/caseManagerAction!quickQuery.action?dto.testCaseInfo.testCaseId="+pmBar.getValue("qSearchIpt");
		var ajaxRest = postSub(url,"");
		if(ajaxRest==""){
			hintMsg("没查到相关记录");
			return;
		}
		pmGrid.clearAll();
		colTypeReset();
		var jsons = eval("(" + ajaxRest +")");
		pmGrid.parse(jsons, "json");
		cusSetPageNoSizeCount();
	   	loadLink();
	}
    function cusSetPageNoSizeCount(){
    	var tBar = pmBar;
		tBar.setItemText("pageP", pageSize);
		tBar.setValue("page", 1);
		tBar.setMaxValue("slider", 1, "");
		tBar.setItemText("pageMessage", "/ " + 1);
		tBar.setValue("slider", 1);
		tBar.disableItem("first");
		tBar.disableItem("pervious");
		tBar.disableItem("next");
		tBar.disableItem("last");
		tBar.disableItem("slider");		
	}
	pmBar.attachEvent("onEnter", function(id, value) {
		if(id=="qSearchIpt"){
			if(!isDigit(pmBar.getValue("qSearchIpt"), false)&&pmBar.getValue("qSearchIpt")!=""){
				pmBar.setValue("qSearchIpt", "");
				return;
			}
			quickQuery();
			return ;
		}
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});

	pmBar.attachEvent("onClick", function(id) {
		if(id == "ldTree"){
			//loadTree();
			parent.mypmLayout.items[1].attachURL(conextPath+"/caseManager/caseManagerAction!loadCase.action");
		}else if(id == "add"){
			if(parent.treeDisModel!="simple"){
				hintMsg("只有普通视图模式才可增加用例");
				return;
			}
			addUpInit('add');
		}else if(id == "upd"){
			if(pmGrid.getSelectedId()==null){
				hintMsg("请点击某条记录而非check来选择一用例");
				return;
			}
			addUpInit('upd');
		}else if(id == "del"){
			delInit();
		}else if(id == 'back'){
			parent.cusBack();
		}else if(id == "adtInit"){
			if(pmGrid.getSelectedId()==null){
				hintMsg("请点击某条记录而非check来选择一用例");
				return;
			}
			adtInit();
		}else if(id == "exe"){
			if(pmGrid.getSelectedId()==null){
				hintMsg("请点击某条记录而非check来选择一用例");
				return;
			}
			addUpInit('exe');
		}else if(id == "viewh"){
			parent.viewHisty();
		}else if(id == "find"){ 
			findInit();
		}else if(id == "first"){
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "last"){
			pageNo = pageCount;
			pageAction(pageNo, pageSize);
		}else if(id == "next"){
			pageNo = pageNo +1
			pageAction(pageNo, pageSize);
		}else if(id == "pervious"){
			pageNo = pageNo -1
			pageAction(pageNo, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}else if(id.length>30&&id!="00000000000000000000000000000000"){
			if($("customCaseHomeTaskId").value==id)
				return;
			parent.parent.mypmMain.location=conextPath+"/caseManager/caseManagerAction!loadCase.action?dto.taskId="+id;
		}else if(id=="sw2Task"){
			swTestTask();
		}else if(id=="expCase"){
			$('currNodeIdF').value=$('currNodeId').value;
			$('findForm').submit();
		}else if(id=="batchAudit"){
			window.location=conextPath+"/caseManager/caseManagerAction!batchAuditInit.action?dto.taskId="+id;
		}else if(id=="reFreshP"){
			pageAction(pageNo, pageSize);
			pmBar.hideItem("cpt");
			reshStatInfo();
		}else if(id=="cpm"){
			if(!copyCaseModel){
				copyCaseModel = true;
				setChkDisableState(false);
				pmBar.setItemToolTip("cpm","切换到普通模式");
				pmBar.setItemImage("cpm","tree.gif");
				pmBar.hideItem("add");
				if(canUp)
				  pmBar.hideItem("upd");
				if(canDel)
				  pmBar.hideItem("del");
				 pmBar.showItem("cp");
				 pmBar.showItem("ct");
				 pmBar.hideItem("cpt");
			}else{
				copyCaseModel = false;
				setChkDisableState(true);
				pmBar.setItemToolTip("cpm","切换到用例复制剪贴模式");
				pmBar.setItemImage("cpm","version.gif");
				pmBar.hideItem("cp");
				pmBar.hideItem("ct");
				pmBar.hideItem("cpt");
				pmBar.showItem("add");
				if(canUp)
				  pmBar.showItem("upd");
				if(canDel)
				  pmBar.showItem("del");
			}
		}else if(id=="cp"){
			pstIds =getChecked(); 
			if(pstIds==""){
				hintMsg("请选择要复制的用例");
				return;				
			}
			resourceId=$("currNodeId").value;
			cpModel="cp";
			pmBar.showItem("cpt");
		}else if(id=="ct"){
			pstIds =getChecked(); 
			if(pstIds==""){
				hintMsg("请选择要剪贴的用例");
				return;				
			}
			resourceId=$("currNodeId").value;
			cpModel="ct";
			pmBar.showItem("cpt");
		}else if(id=="cpt"){
			pasteCase();
		}else if(id="impCase"){
			impCase();
		}
	});
	
	var caseImp_ch;
	function impCase(){
		
		var url = conextPath+"/jsp/caseManager/importCase.jsp";
		caseImp_ch = initW_ch(caseImp_ch, "", true, 250, 120,'caseImp_ch');
		caseImp_ch.setText("导入用例");	
		caseImp_ch.attachURL(url);
		
		caseImp_ch.show();
		caseImp_ch.bringToTop();
		caseImp_ch.setModal(true);			
	}
	function pasteCase(){
		if(cpModel==""){
			hintMsg("请先复制或剪贴");
			return;		
		}
		if(pstIds==""){
			hintMsg("请选择要审批的用例");
			return;
		}
		if(resourceId==$("currNodeId").value&&cpModel=="ct"){
			hintMsg("剪贴时源测试需求和目标测试需求不能相同");
			return;		
		}
		if($("currNodeId").value==parent.mypmLayout.items[0]._frame.contentWindow.rootId){
			hintMsg("目标测试需求不能为根测试需求");
			return;				
		}
		var url = conextPath+"/caseManager/caseManagerAction!pasteCase.action?dto.command="+cpModel+"&dto.remark="+pstIds;
	   $("currNodeIdF").value = $("currNodeId").value;
	   var ajaxResut =  postSub(url,"findForm");
	   if(ajaxResut=="failed"){
			hintMsg("操作失败");
			return;	   
	   }else if(ajaxResut=="selCaseDel"){
			hintMsg("所选用例己被删除");
			return;		   
	   }else if(ajaxResut=="nodeHaveDel"){
			hintMsg("测试测试需求项己被删除");
			return;		   
	   }else if(ajaxResut=="noCaseSel"){
			hintMsg("没选择任何用例");
			return;		   
	   }
	   var userJson = ajaxResut.split("$");
	   if(userJson[0] == "noDelA"){
			hintMsg("所剪贴的用例都有执行记录，实际执行的是复制操作");
	   }else if(userJson[0]=="noDelP"){
	   		hintMsg("部分剪贴的用例有执行记录，这些用例执行复制操作");
	   }
	   pmGrid.clearAll();
	   colTypeReset();
	   pmGrid.parse(eval("(" + userJson[2] +")"), "json");
	   setPageNoSizeCount(userJson[1]);
	   loadLink();
	   if(cpModel=="ct"){
	       pmBar.hideItem("cpt");
	    }	
	}
	function getChecked(){
		selectItems = "";
		var allItems = pmGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			if(items[i] != "" && pmGrid.cells(items[i], 0).getValue() == 1){
				if (selectItems == ""){
					selectItems = items[i];
				}else{
					selectItems += "_" + items[i];
				}
			}
		}
		return selectItems;
	}
	function reshStatInfo(){
	 	var url = conextPath+"/caseManager/caseManagerAction!getCaseStatInfo.action?dto.currNodeId="+$("currNodeId").value;
	    ajaxResut =  postSub(url,"");
	    $("countStr").innerHTML =  ajaxResut;
	}
	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount){
			pmBar.setValue("page", pageNo);
			return ;
		}
		
		var purl = pageBreakUrl + "?dto.isAjax=true&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		//purl+="&dto.currNodeId="+$("currNodeId").value;
		//
		//alert(purl);
		var ajaxRest = postSub(purl,"findForm");
		
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
			return;
		}else{
			pmGrid.clearAll();
			colTypeReset();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		setPageNoSizeCount(userJson[0]);
	   		//setRowNum(pmGrid);
	   		loadLink();
   		}
   		return;
	}
	function exeCase(rest,restStr){
		if($("sel_vers").value=="-1"){
			hintMsg("请选择执行版本");
			return ;
		}else if(isWhitespace($("remark").value)&&(rest=='4'||rest=='5')){
			hintMsg("请填写备注");
			return ;		
		}
		var url=conextPath+"/caseManager/caseManagerAction!exeCase.action?dto.testCaseInfo.testCaseId="+$("testCaseId").value;
		url+="&dto.testCaseInfo.moduleId="+$("moduleId").value+"&dto.testCaseInfo.testStatus="+rest+"&dto.exeVerId="+$("sel_vers").value;
		//url+="&dto.remark="+$("remark").value; 
		$("exeRemark").value= $("remark").value	;
		ajaxResut = postSub(url,"exeCaseForm");
	    if(ajaxResut=="success"){	
	    	pmGrid.cells($("testCaseId").value,5).setValue(restStr);
	    	pmGrid.cells($("testCaseId").value,8).setValue(myName);
			$("exeRemark").value ="";
			dW_ch.hide();
			dW_ch.setModal(false);
	    }else{
	    	hintMsg("保存数据发生错误");
	    }		
	}

	function adtInit(){
		var adtCaseId = pmGrid.getSelectedId();
		if(adtCaseId==null){
			hintMsg("请点击某条记录而非check来选择一用例");
			return;
		}
		$("auditTr").style.display="";
		$("auditoK_b").style.display="";
		$("auditRf_b").style.display="";
		adjustTable("createTable");
		url = conextPath+"/caseManager/caseManagerAction!upInit.action?dto.testCaseInfo.testCaseId="+adtCaseId;	
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="failed"){
			hintMsg("初始化失败");
			return;
		}	
		setUpInfo(ajaxResut);	
		dW_ch = initW_ch(dW_ch,  "createDiv", true,700, 440);
		dW_ch.setDimension(700, 440);
		dW_ch.button("close").attachEvent("onClick", function(){
			eraseAttach('eraseAllImg');
			dW_ch.setModal(false);
			dW_ch.hide();
		});
		$("pass_b").style.display="none";
		$("failed_b").style.display="none";
		$("block_b").style.display="none";
		$("block2_b").style.display="none";
		$("insertImg").style.display="none";
		$("attachTd").innerHTML="附件:";	
		$("currUpFile").style.display="none";
		$("saveBtn").style.display="";
		dW_ch.setText("审核用例");
		$("cUMTxt").innerHTML =  "&nbsp;";
		loadFCK();
		currCmd = "audit";
	}
	var auditRest = "";
	function adtExe(){
		if(isWhitespace($("adtRemark").value)&&auditRest=="6"){
			hintMsg("请填写备注");
			return;
		}
		var adtCaseId = $("testCaseId").value;
		$("testStatus").value=auditRest;
		var url=conextPath+"/caseManager/caseManagerAction!auditCase.action";
		ajaxResut = postSub(url,"createForm");
	    if(ajaxResut=="success"){
	    	pmGrid.cells(adtCaseId,5).setValue(auditRest=="1"?"未测试":"待修正");
	    	pmGrid.cells(adtCaseId,8).setValue(myName);
	    	hintMsgClose();
			dW_ch.hide();
			dW_ch.setModal(false);	
	    }else{
	    	hintMsg("保存数据发生错误");
	    }
	}
	function cp2Pform(){
		$("priIdP").value=$("priIdF").value;
		$("caseTypeIdP").value=$("caseTypeIdF").value;
		$("testRestP").value=$("testRestF").value;
		$("createrIdP").value=$("createrIdF").value;
		$("testAuditIdP").value=$("testAuditIdF").value;
		$("weightP").value=$("weightF").value;
		$("testCaseDesP").value=$("testCaseDesF").value;
	}
	function pfromReset(){
		$("priIdP").value=-1;
		$("caseTypeIdP").value=-1;
		$("testRestP").value=-1;
		$("createrIdP").value="";
		$("testAuditIdP").value="";
		$("weightP").value="";
		$("testCaseDesP").value="";	
	}

	secuW_ch = initW_ch(secuW_ch,  "", true,700, 430);
	secuW_ch.hide();
	secuW_ch.setModal(false);
	var currCmd = "";
	function addUpInit(comd){
		currCmd = comd;
		if(comd!="add"){
			$('createForm').reset();
		}else{
			var caseTypeIdTemp = $("caseTypeId").value;
			var priIdTemp = $("priId").value;	
			$('createForm').reset();
			$("caseTypeId").value=caseTypeIdTemp;
			$("priId").value = priIdTemp;
		}
		$("currUpFile").value="";
		if(_isIE)
		    $('currUpFile').outerHTML=$('currUpFile').outerHTML;
		$('upStatusBar').innerHTML="";
		$("auditTr").style.display="none";
		$("auditoK_b").style.display="none";
		$("auditRf_b").style.display="none";
		var url="";
		if(ontLineState!="1"){
			hintMsg("当前任务没提交需求分解树,不能执行当前操作");
			return;		
		}
		if(comd=="add"){
			if($("customCaseHomeTaskId").value==""){
				hintMsg("请先切换到某个测试任务中");
				return;
			}
			if($("currNodeId").value==""||treeWin.tree.getParentId($("currNodeId").value)=="0"){
				//loadTree('showMe');
				hintMsg("请从需求分解树中选择非根测试需求项");
				return;
			}
			$("attachUrl").value="";
			$("auditId").value="";
			url = conextPath+"/caseManager/caseManagerAction!addInit.action?dto.currNodeId="+$("currNodeId").value ;		
		}else if(comd=="upd"){
			var caseId = pmGrid.getSelectedId();
			if(caseId== null){
				hintMsg("请点击某条记录而非check来选择一用例");
				return;
			}
			url = conextPath+"/caseManager/caseManagerAction!upInit.action?dto.testCaseInfo.testCaseId="+caseId;	
		}else if(comd=="exe"){
			var caseId = pmGrid.getSelectedId();
			if(caseId== null){
				hintMsg("请点击某条记录而非check来选择一用例");
				return;
			}
			url = conextPath+"/caseManager/caseManagerAction!exeCaseInit.action?dto.testCaseInfo.testCaseId="+caseId;	
		}else{
			url = conextPath+"/caseManager/caseManagerAction!viewDetal.action?dto.testCaseInfo.testCaseId="+pmGrid.getSelectedId();	
		}
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="failed"){
			hintMsg("初始化失败");
		}else if(ajaxResut.indexOf("failed")==0&&ajaxResut.indexOf("^")>0){
			hintMsg(ajaxResut.split("^")[1]);
			pmGrid.deleteRow(pmGrid.getSelectedId());		
		}else{
			dW_ch = initW_ch(dW_ch,  "createDiv", true,700, 430);
			dW_ch.button("close").attachEvent("onClick", function(){
				actFlg= false;
				eraseAttach('eraseAllImg');
				dW_ch.setModal(false);
				dW_ch.hide();
			});
			if(comd=="exe"){
				if(_isIE)
					dW_ch.setDimension(700, 455);
				else
					dW_ch.setDimension(700, 440);
				$("verTr").style.display="";
				loadVers();
			}else{
			   dW_ch.setDimension(700, 430);
			   $("verTr").style.display="none";
			 }
			$("mdPath").value=treeWin.getMyCovPath($("currNodeId").value)+":";
			setUpInfo(ajaxResut);
		}
		if(comd=="exe"){
			$("pass_b").style.display="";
			$("failed_b").style.display="";
			$("block_b").style.display="";
			$("block2_b").style.display="";
			$("saveBtn").value="修改";
			dW_ch.setText("执行用例");
		}else if(comd=="add"){
			$("pass_b").style.display="none";
			$("failed_b").style.display="none";
			$("block_b").style.display="none";
			$("block2_b").style.display="none";
			$("saveBtn").value="增加";
			dW_ch.setText("增加用例---当前测试需求:"+treeWin.tree.getItemText($("currNodeId").value));
		}else{
			$("pass_b").style.display="none";
			$("failed_b").style.display="none";
			$("block_b").style.display="none";
			$("block2_b").style.display="none";
			$("saveBtn").value="修改";	
			if($("testStatus").value=="6"){
				$("auditTr").style.display="";	
				dW_ch.setText("修正用例");
				dW_ch.setDimension(700, 440);
			}else
				dW_ch.setText("修改用例");
		}
		if(comd=="detal"){
			viewDetalSw(true);	
			dW_ch.setText("用例明细");
		}else{
			viewDetalSw(false);	
		}
		$("cUMTxt").innerHTML =  "&nbsp;";
		loadFCK();
		$("testCaseDes").focus();
		if(comd=="add"||comd=="upd"){
			$("insertImg").style.display="";
			$("attachTd").innerHTML="附件/插图片:";
			$("currUpFile").style.display="";
		}else{
			$("insertImg").style.display="none";
			$("attachTd").innerHTML="附件:";	
			$("currUpFile").style.display="none";
		}
		if(comd=="add")
			actFlg= true;
		adjustTable("createTable");
	}
	var verSelData = "";
	function loadVers(){
		if(verSelData!="")
			return;
		var verUrl = conextPath+"/testTaskManager/testTaskManagerAction!loadVerSel.action";
		verSelData  = dhtmlxAjax.postSync(verUrl, "").xmlDoc.responseText;
		$("sel_vers").options.length = 1;
		if(verSelData != ""){
			var options = verSelData.split("^");
			for(var i = 0; i < options.length; i++){
				if(options[i].split(";")[0] != "")
					var selvalue = options[i].split(";")[0] ;
					var selable = options[i].split(";")[1];
					$("sel_vers").options.add(new Option(selable,selvalue));
			}
		}
	}
	
	function viewDetalSw(detal){
		$("testCaseDes").readOnly=detal;
		$("expResult").readOnly=detal;
		$("weight").readOnly=detal;
		$("priId").disabled =detal;
		$("caseTypeId").disabled=detal;
		if(detal){
			$("saveBtn").style.display="none";
			$("insertImg").style.display="none";
		}else{
			$("saveBtn").style.display="";
			$("insertImg").style.display="";
			$("currUpFile").style.display="";
		}
	}
	function setUpInfo(updInfo){
		if(updInfo=="")
			return;
		var updInfos = updInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				if(currInfo[0]=="typeSelStr"||currInfo[0]=="priSelStr"){
					var selStr = currInfo[1];
					var selId="priId";
					if(currInfo[0]=="typeSelStr"){
						selId="caseTypeId";
					}
					loadSel(selId,selStr);	
				}else{
					valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
					eval(valueStr);	
					if(currInfo[0]=="expResult"){
						$("expResultOld").value=currInfo[1];
					}else if(currInfo[0]=="operDataRichText"){
						$("initOpd").value=currInfo[1];
						initImg = getAttaInRichHtm(currInfo[1]);
					}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
						$("currAttach").style.display="";
						$("currAttach").title="附件";
						//$("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);
					}else if(currInfo[0]=="attachUrl"&&currInfo[1]==""){
						$("currAttach").style.display="none";
					}else if(currInfo[0]=="remark")
						$("adtRemark").value= currInfo[1];
				}
			}
		}
	}
	function loadSel(selId,selStr,splStr){
		if(selStr==""){
			return;
		}
		$(selId).options.length = 1;
		var options ;
		if(splStr){
			options = selStr.split(splStr);
		}else{
			options = selStr.split("$");
		}
		for(var i = 0; i < options.length; i++){
			var optionArr = options[i].split(";");
			if(optionArr[0] != "")
				var selvalue = optionArr[0] ;
				var selable = optionArr[1];
				$(selId).options.add(new Option(selable,selvalue));
		}	
	}
	var oEditor ;
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			if($("testCaseId").value == ""){
				oEditor.SetData("<strong>" +$("mdPath").value+"</strong>") ;
				return;
			}
			oEditor.SetData($("initOpd").value) ;
			return;
		}
		importFckJs();
    	var pmEditor = new FCKeditor('operDataRichText') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 200;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('operDataRichText') ;
		if($("testCaseId").value == ""){
			oEditor.SetData("<strong>" +$("mdPath").value+"</strong>") ;
			return;
		}
		oEditor.SetData($("initOpd").value) ;
	}
	function delInit(){
		if(pmGrid.getSelectedId()==null){
			hintMsg("请点击而非check来选择要删除的记录");
			return;
		}
		cfDialog('delExe','确定要删除该记录？');
		return;
	}	
	function delExe(){
		var url = conextPath+"/caseManager/caseManagerAction!delCase.action?dto.isAjax=true";
		url+="&dto.testCaseInfo.testCaseId="+pmGrid.getSelectedId();
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="success"){
			pmGrid.deleteRow(pmGrid.getSelectedId());
			clsoseCfWin();
			return true;
		}else if(ajaxResut=="failed"){
			hintMsg("删除失败");
			return false;	
		}else if(ajaxResut.indexOf("^")>0){
			hintMsg(ajaxResut.split("^")[1]);
			return false;				
		}
		return true;
	}
	function save(){
		if(subChk()){
			$("testData").value=html2PlainText("operDataRichText");
			var testDataV = $("testData").value;
			var mdPath = $("mdPath").value;
			if(mdPath.replace(/\s+$|^\s+/g,"")==testDataV.replace(/\s+$|^\s+/g,"")){
				hintMsg("请填写过程及数据");
				return;
			}
			if("可在过程及数据中用表格形式写预期结果,或单独在此填写"==$("expResult").value){
				$("expResult").value="";
			}
			var url = "";
			if($("testCaseId").value == ""){
				url = conextPath+"/caseManager/caseManagerAction!addCase.action?dto.isAjax=true";	
			}else{
				url = conextPath+"/caseManager/caseManagerAction!upCase.action?dto.isAjax=true";
			}
			var ajaxResut = postSub(url,"createForm");
			if(ajaxResut.indexOf("^") != -1){
				ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
				var result = ajaxResut.split("^");
				if(result[0] == "success"){
					 colTypeReset();
					 //eraseAttach();
					if($("testCaseId").value == "" ){ //新增
						pmGrid.addRow(result[1],result[2],0);
					}else{
						var rowNum = pmGrid.cells(result[1],1).getValue() - 1;
						var createName = pmGrid.cells(result[1],11).getValue();
						pmGrid.deleteRow(result[1]);
						pmGrid._addRow(result[1],result[2],0);
						pmGrid.cells(result[1],11).setValue(createName);
						if(currCmd!="exe"&&currCmd!="audit"){
							dW_ch.hide();
							dW_ch.setModal(false);
						}else{
							$("cUMTxt").innerHTML =  "修改用例成功";	
							//setRowNum(pmGrid);
							loadLink();
							pmGrid.setSelectedRow(pmGrid.getRowId(rowNum));	
							pmGrid.setSizes();	
							return;
						}
					}
					//setRowNum(pmGrid);
					loadLink();
					pmGrid.setSelectedRow(pmGrid.getRowId(rowNum));	
					pmGrid.setSizes();	
					$("cUMTxt").innerHTML =  "操作成功";	
					var mdPathInit = $("mdPath").value;
					var testStatusInit = $("testStatus").value;
					var moduleIdInit = $("moduleId").value;			
					var createrId= $("createrId").value;	                                     
					var caseTypeIdTemp = $("caseTypeId").value;
					var priIdTemp = $("priId").value;	
					var testCaseDesTemp = $("testCaseDes").value;
					$('createForm').reset();
					$("testCaseDes").value=testCaseDesTemp;
					$("caseTypeId").value=caseTypeIdTemp;
					$("priId").value = priIdTemp;
					$("createrId").value = createrId;
					$("weight").value="2";
					$("moduleId").value = moduleIdInit;
					$("testStatus").value =testStatusInit ;
					$("mdPath").value =mdPathInit ;
					$("expResult").value="";
					oEditor.SetData("<strong>" +mdPathInit+"</strong>") ;
					upVarReset();
					$('upStatusBar').innerHTML="";
					$("currUpFile").value="";
					return;
				}else if(result[0]=="failed"&&ajaxResut.indexOf("^")>0){
					hintMsg(result[1]);
					return;	
				}else{
					$("cUMTxt").innerHTML =  "操作失败";
					return;	
				}
			}
			$("cUMTxt").innerHTML =  "操作失败";
			return;	
		}
	}
	function subChk(){
		if($("caseTypeId").value=="-1"){
			hintMsg("请选择用例类型");
			return false;
		}else if($("priId").value=="-1"){
			hintMsg("请选择用例优先级");
			return false;
		}else if(isAllNull($("testCaseDes").value)){
			hintMsg("请填写用例描述");
			return false;
		}else if(checkIsOverLong($("testCaseDes").value,60)==true){
			hintMsg("用例描述不能超过60个字符");
			return false;
		}else if(!isAllNull($("expResult").value)&&checkIsOverLong($("expResult").value,400)){
			hintMsg("预期结果不能超过400个字符");
			return false;
		}else if(oEditor.GetXHTML().trim()=="<br>"||oEditor.GetXHTML().trim()==""){
			hintMsg("请填写过程及数据");
			oEditor.Focus();
			return false;		
		}else if(oEditor.GetXHTML().trim()!=""){
			$("operDataRichText").value=oEditor.GetXHTML().trim();
			var endStr = "";
			if($("operDataRichText").value.endWith("</strong>")){
				$("operDataRichText").value=$("operDataRichText").value.trim("</strong>");
				endStr = "</strong>";
			}
			if($("operDataRichText").value.endWith("</b>")){
				$("operDataRichText").value=$("operDataRichText").value.trim("</b>");
				endStr = "</b>";
			}
			$("operDataRichText").value = $("operDataRichText").value.replace(/\s+$|^\s+/g,"");
			$("operDataRichText").value = $("operDataRichText").value.trim("&nbsp;");
			for(var i=1; i<31;i++){
				$("operDataRichText").value = $("operDataRichText").value.trim("<br>");
				$("operDataRichText").value = $("operDataRichText").value.trim("&nbsp;");	
				$("operDataRichText").value = $("operDataRichText").value.replace(/\s+$|^\s+/g,"");		
			}
			$("operDataRichText").value +=endStr;
			if(checkIsOverLong($("operDataRichText").value,1000)){
				hintMsg("过程及数据不能超过1000个字符");
				return false;				
			}	
		}else if($("weight").value!=""&&!isDigit($("weight").value,false)){
				hintMsg("执行成本只能输入数值");
				return false;			
		}
		return true;
	}
	var DropList ="";
	function findInit(){
		if(DropList!=""){
			fW_ch.show();
			fW_ch.bringToTop();
			return;
		}
		var url = conextPath+"/caseManager/caseManagerAction!initDropList.action?dto.isAjax=true";
		var ajaxResut = postSub(url,"");
		if(ajaxResut.indexOf("success")==0){
			DropList = ajaxResut;
			var restArr = ajaxResut.split("$");
			loadSel("caseTypeIdF",restArr[1],"^");
			loadSel("priIdF",restArr[2],"^");
		}
		fW_ch = initW_ch(fW_ch, "findDiv", true, 550, 150);
		fW_ch.setText("查询---当前测试需求:" +treeWin.tree.getItemText($("currNodeId").value));	
	}
	function findExe(){
		$("currNodeIdF").value = $("currNodeId").value;
		var url = pageBreakUrl + "?dto.isAjax=true";
		var ajaxRest = postSub(url, "findForm");
		if(ajaxRest=="failed"){
			hintMsg("查询失败");	
		}else{
			if(ajaxRest.split("$")[1]!=""){
				$("listStr").value=ajaxRest;
				colTypeReset();
				initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
				loadLink();
				fW_ch.hide();
				fW_ch.setModal(false);
				cp2Pform();
				return;		
			}
			hintMsg("没有查询到相关记录");
		}	
	}
	//var relBugW_ch;
	function relaBug(){
		var testCaseId = pmGrid.getSelectedId();
		var currStatus = pmGrid.cells(testCaseId,5).getValue();
		if(currStatus=='待审核'||currStatus=='不适用'||currStatus=='待修正'){
			 hintMsg("待审核,不适用,待修正状态不能关联");
			return;
		}
		var rowNum = pmGrid.getRowIndex(testCaseId);
		var url=conextPath+"/bugManager/relaCaseAction!loadRelaBug.action?dto.moduleId="+pmGrid.cells2(rowNum,17).getValue()+"&dto.testCaseId="+testCaseId;
		parent.relaBug(url);

	}
	function swTestTask(){	
		var url = onextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action";
		parent.location = url;
	}
	var caseHistory_ch;
	function viewCaseHistory(){
		var testCaseId = pmGrid.getSelectedId();
		var url=conextPath+"/caseManager/caseManagerAction!viewCaseHistory.action?dto.testCaseInfo.testCaseId="+testCaseId;
		caseHistory_ch = initW_ch(caseHistory_ch, "", true, 540, 520,'caseHistory_ch');
		caseHistory_ch.attachURL(url);
		caseHistory_ch.setText("用例历史");	
	    caseHistory_ch.show();
	    caseHistory_ch.bringToTop();
	    caseHistory_ch.setModal(true);		
	}