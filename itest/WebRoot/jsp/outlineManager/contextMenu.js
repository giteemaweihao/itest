	var currCtxDirId = "",CtxDirIdInit="1", contextMenuW_ch;
	function showContextMenu(id,obj){
		currCtxDirId = id;
		if(CtxDirIdInit==currCtxDirId){
			contextMenuW_ch.setModal(false);
			contextMenuW_ch.show();
			contextMenuW_ch.bringToTop();
			contextMenuW_ch.setDimension(120,115);
			return;
		}
		setRightMouseEven();
		contextMenuW_ch.setDimension(120,115);
		CtxDirIdInit = id;
	}
	function setRightMouseEven(){	
		if (_isOpera) {
			document.attachEvent("mouseup", function (e) {
				e.cancelBubble = true;
				e.returnValue = false;
				if (e.button == 0 && e.ctrlKey == true) {
					showDirCtxMenu(e.clientX, e.clientY);
				}
				return false;
			});
		} else if(_isIE){
			document.oncontextmenu=function(){
				showDirCtxMenu(window.event.x, window.event.y);
				window.event.returnValue=false;
			};
		} else {
			document.oncontextmenu = function (e) {
				e = e || event;
				e.cancelBubble = true;
				e.returnValue = false;
				showDirCtxMenu(e.clientX, e.clientY);
				return false;
			};
		}
	}
	function showDirCtxMenu(x,y){
		createCtxMenuDiv();
		contextMenuW_ch = initW_ch(contextMenuW_ch, "ctxMenuWinDiv", false, 120, 115);
		contextMenuW_ch.bringToTop();
		contextMenuW_ch.setText("mark");
		contextMenuW_ch.button("close").attachEvent("onClick", function(){
			document.oncontextmenu=function(){return   false};
		});
		fixCss('ctxMenuTable');
		contextMenuW_ch.setPosition(x, y);
	}
	function fixCss(tableId){
		var ctable=$(tableId);
		for(var i=0; i<ctable.rows.length; i++){
			if(ctable.rows[i].style.display==""){
				ctable.rows[i].className="odd_mypm";
			}
		}
	}	
	function closeCtxMenu(){
		if(typeof contextMenuW_ch !="undefined"){
			contextMenuW_ch.hide();
		}		
	}
	function createCtxMenuDiv(){
		var Createflag = false;
		var ctxMenuWinObj = $("ctxMenuWinDiv");
		if(ctxMenuWinObj != null){
			$("ctxMenuWinDiv").removeChild($("ctxMenuDiv"));
		}else{
			ctxMenuWinObj = document.createElement("div");
			ctxMenuWinObj.className = "gridbox_light";
			ctxMenuWinObj.style.cssText = "border:0px;display:none;";
			ctxMenuWinObj.id = "ctxMenuWinDiv";
			Createflag = true;
		}
		var ctxMenuDivObj = document.createElement("div");
		ctxMenuDivObj.id = "ctxMenuDiv";
		ctxMenuDivObj.className = "objbox";
		ctxMenuDivObj.style.cssText = "overflow:auto;width:100%;z-index:99;";
		ctxMenuWinObj.appendChild(ctxMenuDivObj);
		var ctxMenuTable = document.createElement("table");
		ctxMenuTable.id = "ctxMenuTable";
		ctxMenuTable.style.cssText = "border:0px";
		ctxMenuTable.className = "obj row20px";
		ctxMenuTable.cellspacing = 0;
		ctxMenuTable.cellpadding = 0;
		ctxMenuTable.border = 0;
		ctxMenuTable.width = "100%";
		ctxMenuDivObj.appendChild(ctxMenuTable);
		var tbody = document.createElement("tbody");
		ctxMenuTable.appendChild(tbody);
		var tr, td;
		
		tr = document.createElement("tr");
		td = document.createElement("td");
		td.style.cssText = "padding-left:5px;border-right:0px;";
		td.innerHTML = "<a href=javascript:void(0); onclick=javascript:ctxAction('2');>用例设计完成</a>";
		tr.appendChild(td);
		tbody.appendChild(tr);
		tr = document.createElement("tr");
		td = document.createElement("td");
		td.style.cssText = "padding-left:5px;border-right:0px;";
		td.innerHTML = "<a href=javascript:void(0); onclick=javascript:ctxAction('3');>需求不明阻塞</a>";
		tr.appendChild(td);
		tbody.appendChild(tr);
		tr = document.createElement("tr");
		td = document.createElement("td");
		td.style.cssText = "padding-left:5px;border-right:0px;";
		td.innerHTML = "<a href=javascript:void(0); onclick=javascript:ctxAction('4');>完成部分用例</a>";
		tr.appendChild(td);
		tbody.appendChild(tr);

		tr = document.createElement("tr");
		td = document.createElement("td");
		td.style.cssText = "padding-left:5px;border-right:0px;";
		td.innerHTML = "<a href=javascript:void(0); onclick=javascript:ctxAction('5');>需求待定中</a>";
		tr.appendChild(td);
		tbody.appendChild(tr);
		tr = document.createElement("tr");
		td = document.createElement("td");
		td.style.cssText = "padding-left:5px;border-right:0px;";
		td.innerHTML = "&nbsp;";
		tr.appendChild(td);
		tbody.appendChild(tr);
		if(Createflag)document.body.appendChild(ctxMenuWinObj);
	}
	function ctxAction(cmd){
		var url = conextPath+"/outLineManager/outLineAction!setReqState.action?dto.moduleState="+cmd+"&dto.currNodeId="+currCtxDirId;
		var ajaxResut  =postSub(url, "");
		if(ajaxResut=="failed"){
			parent.mypmLayout.items[1]._frame.contentWindow.hintMsg("操作失败");
		}else if(ajaxResut=="haveDel"){
			parent.mypmLayout.items[1]._frame.contentWindow.hintMsg("当前测试需求项己被删除");
		}else if(ajaxResut=="haveStop"){
			parent.mypmLayout.items[1]._frame.contentWindow.hintMsg("当前测试需求项己停用不能标识");
		}else{
			setColorTip(currCtxDirId ,tree.getItemText(currCtxDirId),parseInt(cmd));
		}
		contextMenuW_ch.setModal(false);
		contextMenuW_ch.hide();
	}	