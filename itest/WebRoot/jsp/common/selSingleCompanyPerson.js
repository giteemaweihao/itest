	var callBackCustom;
	var initGroypId="-1";
	var peopleId ;
	var peopleName ;
	var comUserSelW_ch;
	var selGrid;
	var initPassValId = "";
	function popselWin(passValId,passNameId,model,callBack){
		if(typeof callBack == "undefined")
			callBackCustom = "";
		else
			callBackCustom = callBack;
		peopleId = passValId;
		peopleName = passNameId;
		if(typeof comUserSelW_ch !="undefined"&&comUserSelW_ch.isHidden()){
			if(typeof(model) !="undefined"){
				comUserSelW_ch.setModal(model);
			}else{
				comUserSelW_ch.setModal(true);
			}
			markSel();
			comUserSelW_ch.show();
			comUserSelW_ch.bringToTop();
			return;
		}
		if(typeof(importWinJs) != "undefined")
			importWinJs();
		if(typeof(model) !="undefined"){
			if(_isFF)
				comUserSelW_ch = initW_ch(comUserSelW_ch, "selPeopleDiv", true, 190, 380);
			else
			    comUserSelW_ch = initW_ch(comUserSelW_ch, "selPeopleDiv", true, 190, 360);
		}else{
			if(_isFF)
				comUserSelW_ch = initW_ch(comUserSelW_ch, "selPeopleDiv", true, 190, 380);
			else
			    comUserSelW_ch = initW_ch(comUserSelW_ch, "selPeopleDiv", true, 190, 360);
			
			comUserSelW_ch.setModal(true);
		}
		if(typeof(model) =="undefined")
			comUserSelW_ch.button("close").attachEvent("onClick", function(){
				comUserSelW_ch.setModal(false);
				comUserSelW_ch.hide();
			});
		else
			comUserSelW_ch.button("close").attachEvent("onClick", function(){
				comUserSelW_ch.hide();
			});
		comUserSelW_ch.setText("选择人员");
		initSelGrid();//初始化grid
		initGroup("sel_groupIds");//初始化组下拦框
		initSleEdPeople(passValId,passNameId);//初始化grid数据
		comUserSelW_ch.bringToTop();
	}

	
	function initSleEdPeople(passValId,passNameId){
		var seledPeopeId = $(passValId).value;
		var seledPeopeName = $(passNameId).value;
		if(seledPeopeId==""||seledPeopeName==""){
			return;
		}
		if(selGrid.getRowIndex(seledPeopeId)>0){
			selGrid.setRowColor(seledPeopeId, "#CCCCCC");
		}	
	}
	
	function initGroup(id){
		var groupUrl = conextPath+"/userManager/userManagerAction!groupSel.action";
		var groupSel  = dhtmlxAjax.postSync(groupUrl, "").xmlDoc.responseText;
		$(id).options.length = 1;
		if(groupSel != ""){
			var options = groupSel.split("^");
			for(var i = 0; i < options.length; i++){
				if(options[i].split(";")[0] != "")
					var selvalue = options[i].split(";")[0] ;
					var selable = options[i].split(";")[1];
					$(id).options.add(new Option(selable,selvalue));
			}
			if(initGroypId!="-1"){
				$("sel_groupIds").value=initGroypId;
			}
		}
	}

	function loadSelUser(isDefaultLoad){
		initGroypId = $3("sel_groupIds").value;
		var url = conextPath+"/userManager/userManagerAction!selectUser.action?dto.isAjax=true";
		if(typeof isDefaultLoad != "undefined"){
			url = conextPath+"/userManager/userManagerAction!loadDefaultSelUser.action?dto.isAjax=true";
		}
		var ajaxResut  = dhtmlxAjax.postSync(url, "selPeopleForm").xmlDoc.responseText;
		if(ajaxResut=="failed"){
  			if(typeof hintMsg!="undefined"){
  				hintMsg("加载数据发生错误");
  			}else{
  				showMessage(true, "选择人员", "未加载到数据");
  			}
  			return;
  		}
		selGrid.clearAll();
		ajaxResut = ajaxResut.split("$")[1];
	    if(ajaxResut != ""&&ajaxResut!="failed"){
   			ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
   			initData = ajaxResut;
  			selGrid.parse(eval("(" + ajaxResut +")"), "json");
  		}
		markSel('mv');
		return;
	}
	function markSel(mv){
		var seledPeopeId = $(peopleId).value;
		if(seledPeopeId==""){
			return;
		}
 		var allItems = selGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			selGrid.setRowColor(items[i], "#FFFFFF");
		}
		if(selGrid.getRowIndex(seledPeopeId)>=0){
			if(typeof(mv) != "undefined"){
				var userName = selGrid.cells(seledPeopeId,2).getValue();
				selGrid.deleteRow(seledPeopeId);
				selGrid.addRow(seledPeopeId,'0,,'+userName,0);
				selGrid.setRowColor(seledPeopeId, "#CCCCCC");
			}else{
				selGrid.setRowColor(seledPeopeId, "#CCCCCC");
			}
		}			
	}
	function selPeople(){
		if(selGrid.getRowsNum()==0){
			comUserSelW_ch.hide();
			comUserSelW_ch.setModal(false);
			$(peopleId).value = "";
			$(peopleName).value = "" ;
			return;
		}
		$(peopleId).value = selGrid.getSelectedId();
		$(peopleName).value =  selGrid.cells($(peopleId).value,2).getValue();
		comUserSelW_ch.setModal(false);
		comUserSelW_ch.hide();
		if(typeof(callBackCustom) != "undefined" && callBackCustom != ""){
			eval(callBackCustom);
		}
	}
	
	function initSelGrid(){
	    if(typeof selGrid == "undefined"){
		    selGrid = new dhtmlXGridObject('selGridbox');
			selGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			selGrid.setHeader(",,<div title='双击数据行选择'>备选人员 -- 双击选择</div>");
			selGrid.setInitWidths("0,0,*");
	    	selGrid.setColAlign("left,left,left");
	    	selGrid.setColTypes("ro,ro,ro");
	   		selGrid.setColSorting("str,int,str");
	   		selGrid.enableTooltips("false,false,true");
	    	selGrid.enableAutoHeight(true, 240);
	    	selGrid.setMultiselect(true);
	        selGrid.init();
	        selGrid.enableRowsHover(true, "red");
	    	selGrid.setSkin("light");
	    	selGrid.attachEvent("onRowDblClicked",function(rowId,cellInd,state){
	    		selPeople();
			});
			loadSelUser('default');
	    }
	}