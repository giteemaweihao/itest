<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>测试基础数据设置</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>		
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>			
	</HEAD>
	<BODY bgcolor="#ffffff"  style="overflow-x:hidden;">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top" style="padding-right: 3px;">
					<div id="toolbarObj"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox"></div>
				</td>
			</tr>

		</tr>
		</table>
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}"/>
		<ww:hidden id="reProStep" name="reProStep"></ww:hidden>
	<script type="text/javascript">
	ininPage("toolbarObj", "gridbox", 360);
	var testBaseOpts = Array(Array('all', 'obj', '所有'),Array('用例优先级', 'obj', '用例优先级'),Array('用例类型', 'obj', '用例类型'),Array('BUG频率', 'obj', 'BUG频率'),Array('BUG类型', 'obj', 'BUG类型'),Array('BUG等级', 'obj', 'BUG等级'),Array('BUG发现时机', 'obj', 'BUG发现时机'),Array('BUG优先级', 'obj', 'BUG优先级'),Array('BUG来源', 'obj', 'BUG来源'),Array('BUG测试时机', 'obj', 'BUG测试时机'),Array('BUG引入原因', 'obj', 'BUG引入原因'),Array('BUG发生平台', 'obj', 'BUG发生平台'));
	<pmTag:button page="testBaseSet" reFreshHdl = "false"/>
	var myId = "${session.currentUser.userInfo.id}";
	</script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/testBaseSet/testBaseSet.js"></script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="" action="">
				<ww:hidden id="typeId" name="dto.testBaseSet.typeId"></ww:hidden>
				<ww:hidden id="isDefault" name="dto.testBaseSet.isDefault"></ww:hidden>
				<ww:hidden id="status" name="dto.testBaseSet.status"></ww:hidden>
				<ww:hidden id="initSubName" name="dto.testBaseSet.initSubName"></ww:hidden>
				<table border="0" id="createTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="420">
					<tr class="ev_mypm"> 
						<td colspan="4" class="tdtxt" align="center" width="420" style="border-right:0">
							<div id="cUMTxt" align="center"
								style="color: Blue"></div>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td width="100" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							数据类型:
						</td>
						<td width="100" class="tdtxt" align="left" style="border-right:0">
							<ww:select id="subName" name="dto.testBaseSet.subName" cssClass="text_c"
								list="dto.subList" headerKey="all" headerValue=""
								listKey="keyObj" listValue="valueObj"
								cssStyle="width:100;padding:2 0 0 4;"></ww:select>
						</td>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							状态:
						</td>
						<td width="140" class="tdtxt" align="left" style="border-right:0">
						     启用
						</td>
					</tr>
					<tr  class="ev_mypm">
						<td width="100" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0" >
							数据名称:
						</td>
						<td class="tdtxt"  width="100"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="typeName" name="dto.testBaseSet.typeName" cssClass="text_c" maxlength="15"
								cssStyle="width:100;padding:2 0 0 4;"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="border-right:0" >
							备注:
						</td>
						<td class="tdtxt"  width="140"
							style="border-right:0">
							<ww:textfield id="remark" name="dto.testBaseSet.remark" cssClass="text_c" maxlength="20"
								cssStyle="width:140;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr  class="odd_mypm">
					<td class="tdtxt" align="center" width="420" colspan="4" style="border-right:0">
					  <a class="bluebtn" href="javascript:void(0);"onclick="cuW_ch.setModal(false);cuW_ch.hide();"style="margin-left: 6px"><span> 返回</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="saveBtnc" onclick="contin='1';addUpSub()" style="margin-left: 6px"><span>确定并继续</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="contin='';addUpSub()"><span>确定</span> </a>
					<td>
					</tr>
					<tr  class="ev_mypm">
						<td class="tdtxt" align="left" width="420" style="border-right:0">
							&nbsp;
						</td>
					</tr>
			</table>
		</ww:form>
		</div>
		</div>
		<form action="" method="post" id="swTypeForm" name="downForm">
			<input type="hidden" id="sw2TypeId" name="dto.subName" value=""/>
	   </form>				
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</BODY>
</HTML>
