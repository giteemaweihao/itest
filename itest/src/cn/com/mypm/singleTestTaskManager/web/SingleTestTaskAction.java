package cn.com.mypm.singleTestTaskManager.web;

import cn.com.mypm.framework.app.blh.BaseBizLogicHandler;
import cn.com.mypm.framework.exception.BaseException;
import cn.com.mypm.framework.transmission.events.BusiRequestEvent;
import cn.com.mypm.framework.web.action.BaseAction;
import cn.com.mypm.singleTestTaskManager.blh.SingleTestTaskBlh;
import cn.com.mypm.singleTestTaskManager.dto.SingleTestTaskDto;

public class SingleTestTaskAction extends BaseAction<SingleTestTaskBlh> {

	private SingleTestTaskDto dto = new SingleTestTaskDto();
	private SingleTestTaskBlh singleTestTaskBlh ;
	protected void _prepareRequest(BusiRequestEvent reqEvent)
			throws BaseException {
		reqEvent.setDto(dto);

	}

	public SingleTestTaskDto getDto() {
		return dto;
	}

	public void setDto(SingleTestTaskDto dto) {
		this.dto = dto;
	}
	public  BaseBizLogicHandler getBlh(){
		  
		return singleTestTaskBlh;
	}

	public SingleTestTaskBlh getSingleTestTaskBlh() {
		return singleTestTaskBlh;
	}

	public void setSingleTestTaskBlh(SingleTestTaskBlh singleTestTaskBlh) {
		this.singleTestTaskBlh = singleTestTaskBlh;
	}
	

}
