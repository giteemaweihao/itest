package cn.com.mypm.singleTestTaskManager.service.impl;

import java.util.Date;
import java.util.List;

import cn.com.mypm.common.SecurityContextHolderHelp;
import cn.com.mypm.common.util.StringUtils;
import cn.com.mypm.framework.app.services.BaseServiceImpl;
import cn.com.mypm.object.SimpleUser;
import cn.com.mypm.object.SingleTestTask;
import cn.com.mypm.object.TestTaskDetail;
import cn.com.mypm.singleTestTaskManager.dto.SingleTestTaskDto;
import cn.com.mypm.singleTestTaskManager.service.SingleTestTaskService;

public class SingleTestTaskServiceImpl extends BaseServiceImpl implements SingleTestTaskService {

	public void addSingleTest(SingleTestTask singleTest){
		singleTest.setCreateId(SecurityContextHolderHelp.getUserId());
		singleTest.setInsDate(new Date());
		singleTest.setUpdDate(singleTest.getInsDate());
		singleTest.setStatus(3);
		singleTest.setCompanyId(SecurityContextHolderHelp.getCompanyId());
		TestTaskDetail testTaskDetal = new TestTaskDetail();
		testTaskDetal.setCompanyId(singleTest.getCompanyId());
		testTaskDetal.setInsdate(new Date());
		testTaskDetal.setCreateId(singleTest.getCreateId());
		testTaskDetal.setTaskState(3);
		testTaskDetal.setOutlineState(0);
		testTaskDetal.setReltCaseFlag(0);
		testTaskDetal.setCustomBug(0);
		testTaskDetal.setCustomCase(0);
		testTaskDetal.setUpgradeFlag(0);
		this.add(singleTest);
		testTaskDetal.setTaskId(singleTest.getTaskId());
		testTaskDetal.setTestPhase(singleTest.getTestPhase());
		testTaskDetal.setProjectId(singleTest.getTaskId());//非监管测试项目，项目ID和任务ID相同   
		this.add(testTaskDetal);
	}
	
	public void updateSingleTest(SingleTestTask singleTest){

		singleTest.setUpdDate(new Date());
		this.update(singleTest);
	}
	
	public void deleteSingleTest(SingleTestTask singleTest){
		SingleTestTask task = this.get(SingleTestTask.class, singleTest.getTaskId());
		if(task.getStatus()==3){
			this.executeUpdateByHql("delete from  TestTaskDetail where taskId =? and companyId=? ", new Object[]{singleTest.getTaskId(),SecurityContextHolderHelp.getCompanyId()});
			this.executeUpdateByHql("delete from  SingleTestTask where taskId =? and companyId=? ", new Object[]{singleTest.getTaskId(),SecurityContextHolderHelp.getCompanyId()});
		}else{
			task.setStatus(4);
			this.update(task);
			this.executeUpdateByHql("update TestTaskDetail set testTaskState=4 where taskId =? and companyId=?  ", new Object[]{singleTest.getTaskId(),SecurityContextHolderHelp.getCompanyId()});
		}
	}
	
	public SingleTestTask updInit(SingleTestTaskDto dto){
		String hql = "from SingleTestTask s join fetch s.psm where s.taskId=? and s.companyId=?";
		List<SingleTestTask> list = this.findByHql(hql, dto.getSingleTest().getTaskId(),SecurityContextHolderHelp.getCompanyId());
		if(list!=null||!list.isEmpty()){
			SingleTestTask task = list.get(0);
			//不要注掉下面的代码,当缓存过期时,PSM是延迟加载
			if(task.getPsm()==null){
				hql = "from SimpleUser where id=?";
				List<SimpleUser> userList = this.findByHql(hql, task.getPsmId());
				task.setPsm(userList.get(0));
			}
			task.getPsm().getName();
			return task;
		}
		return null;
	}
	
	public String[] getTaskBugDateLimit(String taskId){
		String hql = "select insDate  from BugHandHistory where taskId = ? order by insDate " ;
		List list = this.findByHqlPage(hql, 1, 1, "insDate", taskId);
		Date startDate = null;
		if(list!=null&&!list.isEmpty()){
			startDate = (Date)list.get(0);
		}
		Date endDate = null;
		hql = "select insDate  from BugHandHistory where taskId = ? order by insDate desc" ;
		list = this.findByHqlPage(hql, 1, 1, "insDate", taskId);
		if(list!=null&&!list.isEmpty()){
			endDate = (Date)list.get(0);
		}
		
		if(startDate!=null){
			return new String[]{StringUtils.formatShortDate(startDate),StringUtils.formatShortDate(endDate)};
		}
		return new String[]{"",""};
	}
	
	public String[] getTaskeExeCaseDateLimit(String taskId){
		String hql = "select exeDate  from TestResult where taskId = ? order by exeDate " ;
		//List list = this.findByHql(hql, 1, 1, "exeDate", taskId);
		List list = this.getHibernateGenericController().findByHql(hql, 1, 1, taskId);
		Date startDate = null;
		if(list!=null&&!list.isEmpty()){
			startDate = (Date)list.get(0);
		}
		Date endDate = null;
		hql = "select exeDate  from TestResult where taskId = ? order by exeDate desc" ;
		//list = this.findByHql(hql, 1, 1, "exeDate", taskId);
		list = this.getHibernateGenericController().findByHql(hql, 1, 1, taskId);
		if(list!=null&&!list.isEmpty()){
			endDate = (Date)list.get(0);
		}
		
		if(startDate!=null){
			return new String[]{StringUtils.formatShortDate(startDate),StringUtils.formatShortDate(endDate)};
		}
		return new String[]{"",""};		
	}
	
	public String[] getTaskeWriteCaseDateLimit(String taskId){
		String hql = "select creatdate  from TestCaseInfo where taskId = ? order by creatdate " ;
		//List list = this.findByHql(hql, 1, 1, "exeDate", taskId);
		List list = this.getHibernateGenericController().findByHql(hql, 1, 1, taskId);
		Date startDate = null;
		if(list!=null&&!list.isEmpty()){
			startDate = (Date)list.get(0);
		}
		Date endDate = null;
		hql = "select creatdate  from TestCaseInfo where taskId = ? order by creatdate desc" ;
		//list = this.findByHql(hql, 1, 1, "exeDate", taskId);
		list = this.getHibernateGenericController().findByHql(hql, 1, 1, taskId);
		if(list!=null&&!list.isEmpty()){
			endDate = (Date)list.get(0);
		}
		
		if(startDate!=null){
			return new String[]{StringUtils.formatShortDate(startDate),StringUtils.formatShortDate(endDate)};
		}
		return new String[]{"",""};			
		
	}

}
