package cn.com.mypm.impExpManager.service;

import java.util.List;

import cn.com.mypm.bugManager.dto.BugManagerDto;
import cn.com.mypm.caseManager.dto.CaseManagerDto;
import cn.com.mypm.framework.app.services.BaseService;
import cn.com.mypm.object.OutlineInfo;

public interface ImpExpManagerService extends BaseService {

	public void buildBugWhereSql(BugManagerDto dto);
	
	public  String getExpBugSql();
    
	public  String getExpCaseSql();
	
	public void buildCaseWhereSql(CaseManagerDto dto);
	
	public String getBugCountStr(String taskId);
	public String getBugCountStr();
	
	public String getCaseCountStr(String taskId);
	
	public String getCaseCountStr(String taskId,String moduleNum);
	
	public List getOutLineDetailInfo(String taskId,List<OutlineInfo>  loadNodes,boolean onlyNormal);
	
	public List<OutlineInfo> getOutLineInfo(String taskId,List<Long> listId ,boolean onlyNormal);
	
	public String getBugCountStr(String taskId,String moduleNum);
}
