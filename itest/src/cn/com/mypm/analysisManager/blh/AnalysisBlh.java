package cn.com.mypm.analysisManager.blh;

import org.apache.log4j.Logger;

import cn.com.mypm.analysisManager.dto.AnalysisDto;
import cn.com.mypm.analysisManager.service.AnalysisService;
import cn.com.mypm.framework.app.blh.BusinessBlh;
import cn.com.mypm.framework.app.view.View;
import cn.com.mypm.framework.exception.BaseException;
import cn.com.mypm.framework.jms.CommonMessageListener;
import cn.com.mypm.framework.transmission.events.BusiRequestEvent;

public class AnalysisBlh extends BusinessBlh {
	
	private static Logger log = Logger.getLogger(AnalysisBlh.class);
	private AnalysisService analysisService;
	
	public View goAnalysisMain(BusiRequestEvent req) throws BaseException {
		AnalysisDto analysisDto = (AnalysisDto) req.getDto();
		analysisService.goAnalysisMain(analysisDto);
		return getView("goAnalysisMain");
	}

	public AnalysisService getAnalysisService() {
		return analysisService;
	}

	public void setAnalysisService(AnalysisService analysisService) {
		this.analysisService = analysisService;
	}
	

	
	
}
