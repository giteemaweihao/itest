package cn.com.mypm.analysisManager.service;

import cn.com.mypm.analysisManager.dto.AnalysisDto;
import cn.com.mypm.framework.app.services.BaseService;

public interface AnalysisService extends BaseService {
	
	public void goAnalysisMain(AnalysisDto analysisDto);
	
	//public void goAnalysisMain4Mysql(AnalysisDto analysisDto);
}
