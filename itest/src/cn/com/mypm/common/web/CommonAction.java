package cn.com.mypm.common.web;

import java.util.HashMap;

import cn.com.mypm.common.blh.CommonBlh;
import cn.com.mypm.common.dto.CommonDto;
import cn.com.mypm.framework.exception.BaseException;
import cn.com.mypm.framework.transmission.events.BusiRequestEvent;
import cn.com.mypm.framework.web.action.BaseAction;

public class CommonAction extends BaseAction<CommonBlh> {

	private CommonDto dto = new CommonDto();
	@Override
	protected void _prepareRequest(BusiRequestEvent reqEvent)
			throws BaseException {
		reqEvent.setDto(dto);
	}

	@Override
	protected String _processResponse() throws BaseException {
		HashMap<?, ?> displayData = (HashMap<?, ?>) _getDisplayData();
		return forwardPage(displayData);
	}

	public CommonDto getDto() {
		return dto;
	}

	public void setDto(CommonDto dto) {
		this.dto = dto;
	}

}
