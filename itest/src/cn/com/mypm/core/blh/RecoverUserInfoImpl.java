package cn.com.mypm.core.blh;

import org.apache.log4j.Logger;

import cn.com.mypm.framework.app.blh.RecoverUserInfo;
import cn.com.mypm.framework.exception.BaseException;
import cn.com.mypm.framework.hibernate.HibernateGenericController;
import cn.com.mypm.framework.security.SecurityPrivilege;
import cn.com.mypm.framework.security.Visit;
import cn.com.mypm.framework.security.VisitUser;
import cn.com.mypm.framework.security.filter.SecurityContextHolder;
import cn.com.mypm.framework.transmission.events.RequestEvent;
import cn.com.mypm.object.User;

public class RecoverUserInfoImpl implements RecoverUserInfo {

	private static Logger logger = Logger.getLogger(RecoverUserInfoImpl.class);

	public HibernateGenericController hibernateGenericController;

	public SecurityPrivilege securityPrivilege;

	public void recoverUserInfo(String userId){
		User user = hibernateGenericController.get(User.class, userId);
		if(user!=null){
			VisitUser vu = user.copy2VisitUser();
			securityPrivilege.setUserPrivilege(vu);
			if(logger.isInfoEnabled()){
				logger.info("recoverUserInfo==============:"+userId);
			}
			Visit visit = new Visit();
			visit.setUserInfo(vu);
			SecurityContextHolder.getContext().setVisit(visit);
		}
	}
	/**
	 * 如session 丢�??恢复session中用户信??	 * 
	 * @param req
	 */
	public void recoverUserInfo(RequestEvent req) throws BaseException {

		
		String uUseoeIrpIfeeId = req.getDto().getOperationId();
		if(uUseoeIrpIfeeId==null){
			return;
		}
		if (req.getDto() == null) {
			return;
		}
		

		if (SecurityContextHolder.getContext().getVisit() == null
				&& uUseoeIrpIfeeId != null) {
			User user = hibernateGenericController.get(User.class, uUseoeIrpIfeeId);
			if (user == null) {
				logger.error("======according uUseoeIrpIfeeId : " + uUseoeIrpIfeeId
						+ " can't recorver userInfo in session when invoke "
						+ req.getBlhName() + " method of "
						+ req.getDealMethod() + "===================");
				throw new BaseException("overdue", true);
			}
			VisitUser vu = user.copy2VisitUser();
			securityPrivilege.setUserPrivilege(vu);
			Visit visit = new Visit();
			visit.setUserInfo(vu);
			SecurityContextHolder.getContext().setVisit(visit);
		} else if (SecurityContextHolder.getContext().getVisit() == null
				&& uUseoeIrpIfeeId == null) {
			if (logger.isInfoEnabled()) {
				logger
						.info("======can't recorver userInfo in session bceause no uUseoeIrpIfeeId when invoke  "
								+ req.getBlhName()
								+ " method of "
								+ req.getDealMethod() + "===================");
			}

		}
	}

	public HibernateGenericController getHibernateGenericController() {
		return hibernateGenericController;
	}

	public void setHibernateGenericController(
			HibernateGenericController hibernateGenericController) {
		this.hibernateGenericController = hibernateGenericController;
	}

	public SecurityPrivilege getSecurityPrivilege() {
		return securityPrivilege;
	}

	public void setSecurityPrivilege(SecurityPrivilege securityPrivilege) {
		this.securityPrivilege = securityPrivilege;
	}


}
