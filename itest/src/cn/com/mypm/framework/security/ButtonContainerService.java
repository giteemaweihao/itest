package cn.com.mypm.framework.security;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import cn.com.mypm.framework.hibernate.HibernateGenericController;

/**
 * 
 * @author liuyg
 *initFunctionContainer 方法以后要修改,己标注
 */
public class ButtonContainerService {
	
	private static Logger logger = Logger
			.getLogger(ButtonContainerService.class);
	protected static Map<String, List<Button>> container = new HashMap<String, List<Button>>();
	public static Map<String, List<Button>> functionContainer = new HashMap<String, List<Button>>();
	


	protected static String imagesDirec ;


	public HibernateGenericController hibernateGenericController;
	

	public ButtonContainerService() {
		initContainer();
	}

	public ButtonContainerService(
			HibernateGenericController hibernateGenericController) {

		this.hibernateGenericController = hibernateGenericController;
		initContainer();
		
	}


	private void initContainer() {

		URL buttonSetting = Thread.currentThread().getContextClassLoader().getResource("button.properties");
		Properties settings = new Properties();
		if (buttonSetting != null) {
			try {
				settings.load(buttonSetting.openStream());
			} catch (IOException e) {
				logger.error("fatal eror occur************ Can't load button setting ,make sure button.properties is right***********");
				logger.error(e);
				//e.printStackTrace();
			}
		}

		if (settings.isEmpty()) {
			logger.error("fatal eror occur************  button.properties is empty ***********");
		} else {
			Iterator it = settings.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry me = (Map.Entry) it.next();
				String modelInfo = (String) me.getKey();
				String functionName = modelInfo.substring(modelInfo
						.lastIndexOf(".") + 1);
				String modelPath = modelInfo.substring(0, modelInfo
						.lastIndexOf("/"));
				String checkUrl = modelInfo.substring(modelInfo.lastIndexOf("/")+1, modelInfo
						.lastIndexOf("."));
				String buttonInfo = (String) me.getValue();
				String[] buttonInfoS = buttonInfo.split(",");
				if("onlyPower".equalsIgnoreCase(buttonInfoS[1])){
					buttonInfoS[1] = null;
				}else if("empty".equalsIgnoreCase(buttonInfoS[1])){
					buttonInfoS[1] = "";
				}
				Button button = null;
				if ("s".equalsIgnoreCase(buttonInfoS[2])) {
					button = new Button(buttonInfoS[0], functionName,
							buttonInfoS[1], true, Integer
									.parseInt(buttonInfoS[3]),checkUrl);
				} else {
					button = new Button(buttonInfoS[0], functionName,
							buttonInfoS[1], false, Integer
									.parseInt(buttonInfoS[3]),checkUrl);
				}
				if (container.containsKey(modelPath)) {
					List<Button> list = container.get(modelPath);
					if(!list.contains(button)){
						list.add(button);
					}
					
				} else {
					List<Button> list = new ArrayList<Button>();
					if(!list.contains(button)){
						list.add(button);	
					}
					container.put(modelPath, list);
				}
			}
			sort(container ) ;
		}
	}

	private void sort(Map<String, List<Button>> container ){
		Iterator it = container.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, List<Button>> me = 	(Map.Entry<String, List<Button>>)it.next() ;
			List<Button> list = me.getValue();
			Button[] buttons = new Button[list.size()];
			java.util.Arrays.sort(list.toArray(buttons), new ButtonComparator());
			list.clear();
			for(Button but :buttons){
				list.add(but);
			}
			container.put(me.getKey(), list);
		}
		
	}


	
	class ButtonComparator implements Comparator{
		
	    public int compare(Object o1, Object o2) {
	        Integer key1;
	        Integer key2;
	        if (o1 instanceof Button) {
	            key1 = ((Button)o1).getSeq();
	        }else{
	        	key1 = o1.hashCode();
	        }
	        if (o2 instanceof Button) {
	            key2 =((Button)o2).getSeq();
	        }else{
	        	key2 = o2.hashCode();
	        }
	        return key1.compareTo(key2);
	    }
	}

	public String getImagesDirec() {
		return imagesDirec;
	}

	public void setImagesDirec(String imagesDirec) {
		this.imagesDirec = imagesDirec;
	}






}
