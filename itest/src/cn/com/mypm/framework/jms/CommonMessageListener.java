package cn.com.mypm.framework.jms;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.apache.log4j.Logger;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import cn.com.mypm.common.SecurityContextHolderHelp;
import cn.com.mypm.framework.common.ResourceUtils;
import cn.com.mypm.framework.hibernate.HibernateGenericController;
import cn.com.mypm.framework.security.SysLog;
import cn.com.mypm.msgManager.dto.MailBean;

/**
 * @author liuyg
 * 
 */
public class CommonMessageListener {

	private static Logger logger = Logger.getLogger(CommonMessageListener.class);
	
	
	JavaMailSender mailSender;

	
	public CommonMessageListener(){
		//reptInit();
	}
	
	public void listener(Object obj) {
		if(obj instanceof SysLog ){

		}else{
			sendMail((MailBean)obj);
		}
	}

	private void sendMail(MailBean mailBean){
		if((mailBean.getAttachPhName()!=null&&mailBean.getAttachPhName().length>0)||mailBean.isMimeMail()){
			sendMimeMail(mailBean);
			return;
		}
		sendSimpleMail(mailBean);
	}
	
	private void sendSimpleMail(MailBean mailBean){
		if(mailBean.getRecip()==null||"".equals(mailBean.getRecip().trim())){
			return;
		}
		SimpleMailMessage mail = new SimpleMailMessage();  
		JavaMailSenderImpl sendImp = (JavaMailSenderImpl)mailSender;
		mail.setFrom(sendImp.getUsername());  
		mail.setTo(mailBean.getRecip().split(";"));  
		mail.setSubject(mailBean.getSubject());  
		mail.setText(mailBean.getMsg());  
		mailSender.send(mail);
		
	}
	private void sendMimeMail(MailBean mailBean){
		if(mailBean.getRecip()==null||"".equals(mailBean.getRecip().trim())){
			return;
		}
		MimeMessage mm  =mailSender.createMimeMessage();
		MimeMessageHelper mmh = null;
		JavaMailSenderImpl sendImp = (JavaMailSenderImpl)mailSender;
		try {
			mmh = new MimeMessageHelper(mm,true,"utf-8");
			mmh.setSubject(mailBean.getSubject()) ;   
			mmh.setText("<html><head></head><body>"+mailBean.getMsg()+"</body></html>",true);  
			mmh.setFrom(sendImp.getUsername());
			mmh.setTo(mailBean.getRecip().split(";"));
			if(mailBean.getAttachPhName()!=null){
				String upDirectory = SecurityContextHolderHelp.getUpDirectory();
				for(String phName :mailBean.getAttachPhName()){
					URL url = ResourceUtils.getFileURL(upDirectory+File.separator+phName);
					File file = new File(url.getFile());
					if(phName.indexOf("/")==phName.lastIndexOf("/")){
						String flieName =MimeUtility.encodeWord(phName);
						mmh.addAttachment(flieName,file); 					
					}else{
						String flieName =MimeUtility.encodeWord(phName.substring(phName.lastIndexOf("/")));
						mmh.addAttachment(flieName,file); 	
					}			
				}
			}
			mailSender.send(mm);
			
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
			//e.printStackTrace();
		} catch (MessagingException e) {
			logger.error(e);
			//e.printStackTrace();
		} 
		
	}	


	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}



	

}