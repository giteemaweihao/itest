package cn.com.mypm.testBaseSet.service;

import cn.com.mypm.framework.app.services.BaseService;
import cn.com.mypm.object.TypeDefine;
import cn.com.mypm.testBaseSet.dto.TestBaseSetDto;

public interface TestBaseSetService extends BaseService{
	
	public TypeDefine chgTypeDefine(TestBaseSetDto dto);

}
