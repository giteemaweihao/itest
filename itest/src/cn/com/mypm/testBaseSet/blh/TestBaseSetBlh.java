package cn.com.mypm.testBaseSet.blh;

import java.util.Date;
import java.util.List;

import cn.com.mypm.common.SecurityContextHolderHelp;
import cn.com.mypm.framework.app.blh.BusinessBlh;
import cn.com.mypm.framework.app.view.View;
import cn.com.mypm.framework.transmission.events.BusiRequestEvent;
import cn.com.mypm.object.TypeDefine;
import cn.com.mypm.testBaseSet.dto.TestBaseSetDto;
import cn.com.mypm.testBaseSet.dto.TestBaseSetVo;
import cn.com.mypm.testBaseSet.service.TestBaseSetService;

public class TestBaseSetBlh extends BusinessBlh {

	private TestBaseSetService testBaseSetService;
	public View testBaseSetList(BusiRequestEvent req){
		
		TestBaseSetDto dto = super.getDto(TestBaseSetDto.class, req);
		String hql = "from TypeDefine where (compId=? or compId=1) and status!=3  order by updDate,isDefault desc ";
		if(dto.getSubName()!=null&&!"all".equals(dto.getSubName())&&!"".equals(dto.getSubName())){
			if(dto.getTestBaseSet()==null){
				dto.setTestBaseSet(new TestBaseSetVo());
			}
			hql = hql.replaceAll("TypeDefine", dto.getTestBaseSet().getSubClass(dto.getSubName()).getSimpleName());
		}
		List<TypeDefine> typeList = testBaseSetService.findByHqlPage(hql, dto.getPageNo(), dto.getPageSize(), "typeId", SecurityContextHolderHelp.getCompanyId()); 
		if(typeList!=null&&!typeList.isEmpty()){
			for(TypeDefine td :typeList){
				td.setSubName();
			}
		}
		List list = typeList;
		StringBuffer sb = new StringBuffer();
		dto.toJson2(list, sb);
		if(dto.getIsAjax()!=null){
			//System.out.println(JsonUtil.toJson(list));
			super.writeResult(sb.toString());
			return super.globalAjax();
		}
		dto.setListStr(sb.toString());
		return super.getView();
	}

	public View add(BusiRequestEvent req){
		TestBaseSetDto dto = super.getDto(TestBaseSetDto.class, req);
		String reNameHql = "from " +dto.getTestBaseSet().getSubClass(dto.getTestBaseSet().getSubName()).getSimpleName();
		reNameHql = reNameHql + " where typeName=? and status!=3" ;
		Long count = testBaseSetService.getHibernateGenericController().getResultCount(reNameHql, new Object[]{dto.getTestBaseSet().getTypeName()}, "typeId");
		if(count>0){
			super.writeResult("reName");
			return super.globalAjax();			
		}
		TypeDefine td = dto.getTestBaseSet().copy2TypeDefine();
		td.setCompId(SecurityContextHolderHelp.getCompanyId());
		td.setUpdDate(new Date());
		testBaseSetService.add(td);
		super.writeResult("success$"+td.toStrUpdateRest());
		return super.globalAjax();
	}
	
	public View update(BusiRequestEvent req){
		TestBaseSetDto dto = super.getDto(TestBaseSetDto.class, req);
		Long id = dto.getTestBaseSet().getTypeId();
		String hql = "from TypeDefine where typeId=? and compId=? and isDefault=0 ";
		List<TypeDefine> list = testBaseSetService.findByHql(hql, id,SecurityContextHolderHelp.getCompanyId());
		TypeDefine td = list.get(0);
		td.setSubName();
		if(!dto.getTestBaseSet().getInitSubName().equals(dto.getTestBaseSet().getSubName())||!dto.getTestBaseSet().getInitSubName().equals(td.getSubName())){
			String reNameHql = "from " +dto.getTestBaseSet().getSubClass(dto.getTestBaseSet().getSubName()).getSimpleName();
			reNameHql = reNameHql + " where typeName=? and status!=3" ;
			Long count = testBaseSetService.getHibernateGenericController().getResultCount(reNameHql, new Object[]{dto.getTestBaseSet().getTypeName()}, "typeId");
			if(count>0){
				super.writeResult("reName");
				return super.globalAjax();			
			}
			TypeDefine newTd = testBaseSetService.chgTypeDefine(dto);
			super.writeResult("success$"+newTd.toStrUpdateRest());
			return super.globalAjax();		
		}else{
			String reNameHql = "from " +dto.getTestBaseSet().getSubClass(dto.getTestBaseSet().getSubName()).getSimpleName();
			reNameHql = reNameHql + " where typeName=? and typeId!=? and status!=3" ;
			Long count = testBaseSetService.getHibernateGenericController().getResultCount(reNameHql, new Object[]{dto.getTestBaseSet().getTypeName(),dto.getTestBaseSet().getTypeId()}, "typeId");
			if(count>0){
				super.writeResult("reName");
				return super.globalAjax();			
			}
			td.setUpdDate(new Date());
			td.setIsDefault(0);
			td.setRemark(dto.getTestBaseSet().getRemark().trim());
			td.setTypeName(dto.getTestBaseSet().getTypeName().trim());
			super.writeResult("success$"+td.toStrUpdateRest());
			testBaseSetService.update(td);
		}
		return super.globalAjax();
	}

	public View updInit(BusiRequestEvent req){
		TestBaseSetDto dto = super.getDto(TestBaseSetDto.class, req);
		Long id = dto.getTestBaseSet().getTypeId();
		String hql = "from TypeDefine where typeId=? and compId=? and isDefault=0";
		List<TypeDefine> list = testBaseSetService.findByHql(hql, id,SecurityContextHolderHelp.getCompanyId());
		if(list==null||list.isEmpty()){
			super.writeResult("NotFind");
		}else{
			super.writeResult("success$"+list.get(0).toStrUpdateInit());
		}
		return super.globalAjax();
	}
	
	public View delete(BusiRequestEvent req){
		TestBaseSetDto dto = super.getDto(TestBaseSetDto.class, req);
		String hql = "update TypeDefine set status = ? ,updDate = ? where typeId=?";
		testBaseSetService.executeUpdateByHql(hql, new Object[]{"3",new Date(),dto.getTestBaseSet().getTypeId()});
		return super.globalAjax();
	}
	public View swStatus(BusiRequestEvent req){
		TestBaseSetDto dto = super.getDto(TestBaseSetDto.class, req);
		String hql = "update TypeDefine set status = ?,updDate=? where typeId=?";
		testBaseSetService.executeUpdateByHql(hql, new Object[]{dto.getTestBaseSet().getStatus(),new Date(),dto.getTestBaseSet().getTypeId()});
		return super.globalAjax();
	}

	
	public TestBaseSetService getTestBaseSetService() {
		return testBaseSetService;
	}

	public void setTestBaseSetService(TestBaseSetService testBaseSetService) {
		this.testBaseSetService = testBaseSetService;
	}
	

}
