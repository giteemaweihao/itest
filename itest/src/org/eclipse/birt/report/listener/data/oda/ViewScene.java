package org.eclipse.birt.report.listener.data.oda;

import cn.com.mypm.framework.exception.BaseException;
import cn.com.mypm.framework.transmission.events.RequestEvent;

public interface ViewScene {

	public boolean recoverUserInfo(RequestEvent req) throws BaseException ;
}
