# itest

#### Description
itest 流程驱动测试管理软件 

官网及介绍  [http:/120.78.0.137:8080](http://)

#### Installation

1.src\resource\spring\configure.properties 数据库配置文 

2.dbScript\itest2.5.sql    数据库脚本（初始itest 的超级管理员为admin ,密码也是admin）

3.itest 2.5 由mypm 2.5 改写而来，只支持JDK 1.7 ，servlet 支持2.5，推荐使用tomcat 8.5 

4. Itest V3+ ，是Itest 专业版，非商业目的，可免费使用；专业版大版本升级后，会把前一专业版大版本合并到开源版中（年底会把3.0源码提交上来）,专业版和开源版的区别在于发布源码有时间差，专业版官网有一键安装包。
#### Instructions

1. 流程驱动测试、度量展现测试人价值的测试协同软件
2. 流程推动缺陷流转，不同的流程对应不同的状态演化，反应不同管控目的，并可实时调整


